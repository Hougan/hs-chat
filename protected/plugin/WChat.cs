﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json;
using Oxide.Core;
using Oxide.Core.Libraries;
using Oxide.Core.Plugins;
using Oxide.Game.Rust.Cui;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("WChat", "Hougan", "0.2.4")]
    [Description("Полноценная чат-система с возможностью интеграции в WEB")]
    public class WChat : RustPlugin
    {
        #region Classes

        private static class WSettings 
        {
            [JsonProperty("Основная ссылка для запросов")]
            public static string WebChatURI = "https://chat.hougan.space"; 
            [JsonProperty("Секретный ключ доступа")]
            public static string SecretKey = "%REPLACE_ID%";   

            public static void Validate() => WebBuffer.RequestOut.ValidateConnect(); 
        }

        private class ValueManager  
        {
            [JsonProperty("Название в интерфейсе")] 
            public string IValue;

            [JsonProperty("Значение в чате (может быть пустым)")] 
            public string CValue;
 
            [JsonProperty("Привилегия (wchat.<name> или пусто)")]
            public string Permission;

            public bool CanUse(BasePlayer player) => Permission == string.Empty || _.permission.UserHasPermission(player.UserIDString, Permission);
        }

        private class PlayerInfo
        {
            [JsonProperty("UID")] public string UserID;

            [JsonProperty("P")]  public ValueManager Prefix;
            [JsonProperty("PC")] public ValueManager PrefixColor;
            [JsonProperty("NC")] public ValueManager NameColor;
            [JsonProperty("H")]  public ValueManager Hook;
            [JsonProperty("MC")] public ValueManager MessageColor;

            [JsonProperty("HN")] public bool Hide;
            [JsonProperty("BC")] public bool Censore;
            [JsonProperty("BS")] public bool SoundPm;
            [JsonProperty("BT")] public bool Tips;

            [JsonProperty("IL")] public List<ulong> Ignores;

            [JsonProperty("UMT")] public double UnMuteTime;
            [JsonIgnore] public double SpamTime;
            [JsonIgnore] public BasePlayer LastPm;

            public bool IsMuted() => UnMuteTime != 0 && UnMuteTime > Time();

            public static PlayerInfo Generate(BasePlayer player)
            {
                return new PlayerInfo
                {
                    UserID = player.UserIDString,

                    Prefix       = Settings.PrefixSettings.Prefixes.FirstOrDefault(p => p.Permission == string.Empty),
                    PrefixColor  = Settings.PrefixSettings.Colors.FirstOrDefault(p => p.Permission == string.Empty),
                    NameColor    = Settings.NameSettings.Colors.FirstOrDefault(p => p.Permission == string.Empty),
                    Hook         = Settings.PrefixSettings.Hooks.FirstOrDefault(p => p.Permission == string.Empty),
                    MessageColor = Settings.MessageSettings.Colors.FirstOrDefault(p => p.Permission == string.Empty),
                    Ignores      = new List<ulong>(),
                    UnMuteTime   = 0,

                    Censore = true,
                    Tips    = true,
                    SoundPm = false,

                    Hide = false
                };
            }
        }

        private class CuiCountdownComponent : ICuiComponent
        {
            [JsonProperty("type")]
            public string Type
            {
                get { return "Countdown"; }
            }

            [JsonProperty("endTime")]   public int    EndTime;
            [JsonProperty("startTime")] public int    StartTime;
            [JsonProperty("step")]      public int    Step;
            [JsonProperty("command")]   public string Command;
        }

        private class PrefixSettings
        {
            [JsonProperty("Доступные для выбора префикмсы")]
            public HashSet<ValueManager> Prefixes = new HashSet<ValueManager>();

            [JsonProperty("Доступные для выбора цвета")]
            public HashSet<ValueManager> Colors = new HashSet<ValueManager>();

            [JsonProperty("Доступные для выбора скобки")]
            public HashSet<ValueManager> Hooks = new HashSet<ValueManager>();
        }

        private class NameSettings
        {
            [JsonProperty("Доступные для выбора цвета")]
            public HashSet<ValueManager> Colors = new HashSet<ValueManager>();
        }

        private class MessageSettings
        {
            [JsonProperty("Доступные для выбора цвета")]
            public HashSet<ValueManager> Colors = new HashSet<ValueManager>();
        }

        private class MuteSettings
        {
            [JsonProperty("Стандартное количество часов")]
            public int DefaultHours = 0;

            [JsonProperty("Стандартное количество минут")]
            public int DefaultMinutes = 30;

            [JsonProperty("Стандартное количество секунд")]
            public int DefaultSeconds = 0;

            [JsonProperty("Возможные причины для репортов")]
            public List<string> Reasons = new List<string>();
        }

        private class Configuration
        {
            [JsonProperty("Визуализация запросов к серверу")]
            public bool Debug;
 
            [JsonProperty("Настройки префиксов")] public PrefixSettings  PrefixSettings;
            [JsonProperty("Настройки имён")]      public NameSettings    NameSettings;
            [JsonProperty("Настройки сообщений")] public MessageSettings MessageSettings;
            [JsonProperty("Настройки мутов")]     public MuteSettings    MuteSettings;

            [JsonProperty("Настройки авто-сообщений")] 
            public HashSet<string> AutoMessages = new HashSet<string>();

            [JsonProperty("Интервал авто-сообщений")]
            public int AutoMessagesInterval = 300;

            [JsonProperty("Интервал между сообщения (в сек., анти-флуд)")]
            public int AntiSpamInterval = 5;
            [JsonProperty("Ссылка на профиль стима (аватар будет использоваться в глоб. сообщениях)")]
            public ulong AvatarId;

            [JsonProperty("Настройки цензуры (и их исключений, принимает формат MoscowOVH)")]
            public Dictionary<string, List<string>> Censures = new Dictionary<string, List<string>>();

            public static Configuration Generate()
            {
                return new Configuration
                {
                    AvatarId = 0,
                    Debug = false,
                    MuteSettings = new MuteSettings
                    {
                        Reasons = new List<string>
                        {
                            "Общие нарушения чата",
                            "Оскорбление",
                            "Клевета"
                        }
                    },
                    PrefixSettings = new PrefixSettings
                    {
                        Colors = new HashSet<ValueManager>
                        {
                            new ValueManager
                            {
                                IValue     = "СТАНДАРТНЫЙ",
                                CValue     = "#FFFFFF",
                                Permission = ""
                            },
                            new ValueManager
                            {
                                IValue     = "КРАСНЫЙ",
                                CValue     = "#eb8a8a",
                                Permission = "wchat.prefixColorRed"
                            },
                            new ValueManager
                            {
                                IValue     = "СВ. ЗЕЛЕНЫЙ",
                                CValue     = "#99eb8a",
                                Permission = "wchat.prefixColorGreen"
                            }
                        },
                        Prefixes = new HashSet<ValueManager>
                        {
                            new ValueManager
                            {
                                IValue     = "ИГРОК",
                                CValue     = "ИГРОК",
                                Permission = ""
                            },
                            new ValueManager
                            {
                                IValue     = "VIP",
                                CValue     = "VIP",
                                Permission = "wchat.prefixVip"
                            },
                            new ValueManager
                            {
                                IValue     = "PREMIUM",
                                CValue     = "PREMIUM",
                                Permission = "wchat.premium"
                            },
                            new ValueManager
                            {
                                IValue     = "PLATINA",
                                CValue     = "PLATINA",
                                Permission = "wchat.platina"
                            },
                            new ValueManager
                            {
                                IValue     = "МОДЕРАТОР",
                                CValue     = "МОДЕРАТОР",
                                Permission = "wchat.moderator"
                            },
                            new ValueManager
                            {
                                IValue     = "СОЗДАТЕЛЬ",
                                CValue     = "СОЗДАТЕЛЬ",
                                Permission = "wchat.admin"
                            },
                        },
                        Hooks = new HashSet<ValueManager>
                        {
                            new ValueManager
                            {
                                IValue     = "СТАНДАРТНЫЙ",
                                CValue     = "%PREFFIX% | ",
                                Permission = ""
                            },
                            new ValueManager
                            {
                                IValue     = "СКОБКИ",
                                CValue     = "[%PREFFIX%]",
                                Permission = "wchat.hooks"
                            },
                            new ValueManager
                            {
                                IValue     = "ОБЫЧНЫЙ",
                                CValue     = "%PREFFIX%",
                                Permission = "wchat.hooks"
                            },
                        }
                    },
                    NameSettings = new NameSettings
                    {
                        Colors = new HashSet<ValueManager>
                        {
                            new ValueManager
                            {
                                IValue     = "СТАНДАРТНЫЙ",
                                CValue     = "#a6db9c",
                                Permission = ""
                            },
                            new ValueManager
                            {
                                IValue     = "СИНИЙ",
                                CValue     = "#4e6df5",
                                Permission = ""
                            },
                            new ValueManager
                            {
                                IValue     = "ФИОЛЕТОВЫЙ",
                                CValue     = "#be4ef2",
                                Permission = "wchat.purple"
                            },
                        }
                    },
                    MessageSettings = new MessageSettings
                    {
                        Colors = new HashSet<ValueManager>
                        {
                            new ValueManager
                            {
                                IValue     = "СТАНДАРТНЫЙ",
                                CValue     = "#e6e6e6",
                                Permission = ""
                            },
                            new ValueManager
                            {
                                IValue     = "КРАСНЫЙ",
                                CValue     = "#eb8a8a",
                                Permission = "wchat.prefixColorRed"
                            },
                            new ValueManager
                            {
                                IValue     = "СВ. ЗЕЛЕНЫЙ",
                                CValue     = "#99eb8a",
                                Permission = "wchat.prefixColorGreen"
                            },
                        }
                    },
                    Censures = new Dictionary<string, List<string>>
                    {
                        {"бля", new List<string>()},
                        {"еб", new List<string>()},
                        {"аху", new List<string>()},
                        {"впиз", new List<string>()},
                        {"въеб", new List<string>()},
                        {"выбля", new List<string>()},
                        {"выеб", new List<string>()},
                        {"выёб", new List<string>()},
                        {"гнид", new List<string>()},
                        {"гонд", new List<string>()},
                        {"promo", new List<string>()},
                        {"промокод", new List<string>()},
                        {"промо", new List<string>()},
                        {"прамакод", new List<string>()},
                        {"прамо", new List<string>()},
                        {"доеб", new List<string>()},
                        {"долбо", new List<string>()},
                        {"дроч", new List<string>()},
                        {"ёб", new List<string>()},
                        {"елд", new List<string>()},
                        {"заеб", new List<string>()},
                        {"заёб", new List<string>()},
                        {"залуп", new List<string>()},
                        {"захуя", new List<string>()},
                        {"заяб", new List<string>()},
                        {"злоеб", new List<string>()},
                        {"ипа", new List<string>()},
                        {"лох", new List<string>()},
                        {"лошар", new List<string>()},
                        {
                            "манд", new List<string>
                            {
                                "мандар"
                            }
                        },
                        {"мля", new List<string>()},
                        {"мраз", new List<string>()},
                        {
                            "муд", new List<string>
                            {
                                "мудр"
                            }
                        },
                        {"наеб", new List<string>()},
                        {"наёб", new List<string>()},
                        {"напизд", new List<string>()},
                        {
                            "нах", new List<string>
                            {
                                "наха",
                                "нахо",
                                "нахл"
                            }
                        },
                        {
                            "нех", new List<string>
                            {
                                "нехо",
                                "нехв",
                                "неха"
                            }
                        },
                        {"нии", new List<string>()},
                        {"обоср", new List<string>()},
                        {"отпиз", new List<string>()},
                        {"отъеб", new List<string>()},
                        {"оху", new List<string>()},
                        {"падл", new List<string>()},
                        {"падон", new List<string>()},
                        {"педр", new List<string>()},
                        {"пез", new List<string>()},
                        {"перд", new List<string>()},
                        {
                            "пид", new List<string>
                            {
                                "пидж"
                            }
                        },
                        {"пиз", new List<string>()},
                        {"подъеб", new List<string>()},
                        {"поеб", new List<string>()},
                        {"поёб", new List<string>()},
                        {"похе", new List<string>()},
                        {"похр", new List<string>()},
                        {
                            "поху", new List<string>
                            {
                                "похуд"
                            }
                        },
                        {"придур", new List<string>()},
                        {"приеб", new List<string>()},
                        {"проеб", new List<string>()},
                        {"разху", new List<string>()},
                        {"разъеб", new List<string>()},
                        {"распиз", new List<string>()},
                        {
                            "соси", new List<string>
                            {
                                "сосиск"
                            }
                        },
                        {"спиз", new List<string>()},
                        {"сук", new List<string>()},
                        {"суч", new List<string>()},
                        {"трах", new List<string>()},
                        {"ублю", new List<string>()},
                        {"уеб", new List<string>()},
                        {"уёб", new List<string>()},
                        {
                            "ху", new List<string>
                            {
                                "худ",
                                "хуж",
                                "хут",
                                "хур",
                                "хулиг"
                            }
                        },
                        {"целка", new List<string>()},
                        {
                            "чмо", new List<string>
                            {
                                "чмок"
                            }
                        },
                        {"шалав", new List<string>()},
                        {"шлюх", new List<string>()}
                    }
                };
            }
        }

        private class WebBuffer
        {
            internal class RequestIn
            {
                [JsonProperty("requestType")]   public string                     Type   = "UNKNOWN";
                [JsonProperty("requestParams")] public Dictionary<string, object> Params = new Dictionary<string, object>();

                public void ExecuteRequest()
                {
                    switch (Type)
                    {
                        case "in_userMuteRequest":
                        {
                            if (!Params.ContainsKey("id") || !Params.ContainsKey("targetId") || !Params.ContainsKey("time") || !Params.ContainsKey("reason"))
                            {
                                _.PrintError($"Неправильные аргументы [{Type}]");
                                foreach (var arg in Params)
                                    _.PrintError($" - {arg.Key} -> {arg.Value}");
                                return;
                            }

                            string id       = Params["id"].ToString();
                            string targetId = Params["targetId"].ToString();
                            int    time     = int.Parse(Params["time"].ToString());
                            string reason   = Params["reason"].ToString();

                            RequestOut.SendUserMuted(id, targetId, time, reason);
                            break;
                        }
                        case "in_userInfoRequest":
                        {
                            if (!Params.ContainsKey("id") || !Params.ContainsKey("targetId"))
                            {
                                _.PrintError($"Неправильные аргументы [{Type}]");
                                foreach (var arg in Params)
                                    _.PrintError($" - {arg.Key} -> {arg.Value}");
                                return;
                            }

                            string id       = Params["id"].ToString();
                            string targetId = Params["targetId"].ToString();

                            RequestOut.SendUserInfo(id, targetId);
                            break;
                        }
                        case "in_chatHistoryRequest":
                        {
                            if (!Params.ContainsKey("id") || !Params.ContainsKey("amount"))
                            {
                                _.PrintError($"Неправильные аргументы [{Type}]");
                                foreach (var arg in Params)
                                    _.PrintError($" - {arg.Key} -> {arg.Value}");
                                return;
                            }

                            string id     = Params["id"].ToString();
                            int    amount = int.Parse(Params["amount"].ToString());

                            RequestOut.SendLogs(id, amount);
                            break;
                        }
                        case "in_sendMessageRequest":
                        {
                            if (!Params.ContainsKey("id") || !Params.ContainsKey("message"))
                            {
                                _.PrintError($"Неправильные аргументы [{Type}]");
                                foreach (var arg in Params)
                                    _.PrintError($" - {arg.Key} -> {arg.Value}");
                                return;
                            }

                            string id       = Params["id"].ToString();
                            string targetId = string.Empty;
                            if (Params.ContainsKey("target") && Params["target"].ToString() != string.Empty)
                                targetId = Params["target"].ToString();

                            string message = Params["message"].ToString();

                            if (targetId == string.Empty)
                            {
                                _.Broadcast($"SERVER: " + message); 
                                _.AddMessage("SERVER", 0, message);
                            }
                            else
                            {
                                _.SendPrivateMessage(0, ulong.Parse(targetId), message);
                            }

                            RequestOut.SendMessageGlobal(id);
                            break;
                        }
                    }
                }
            }

            internal class RequestOut
            {
                [JsonProperty("secretKey")]     public string                     SecretKey = WSettings.SecretKey;
                [JsonProperty("requestType")]   public string                     Type      = "UNKNOWN";
                [JsonProperty("requestParams")] public Dictionary<string, object> Params    = new Dictionary<string, object>();

                public static void ValidateConnect()
                {
                    RequestOut request = new RequestOut();

                    request.Type = "out_validateVersion";

                    request.Params.Add("id", "123456789");
                    request.Params.Add("version", _.Version.ToString());

                    SendRequest(request, result =>
                    {
                        if (result.Length == 0)
                        {
                            _.PrintWarning("Подключение к веб-интерфейсу успешно!");
                            _.timer.Every(1, GetRequests);
                        }
                        else
                        {
                            _.PrintError(result);
                            UnloadPlugin();
                        }
                    });
                }

                public static void GetRequests()
                {
                    RequestOut request = new RequestOut();

                    request.Type = "out_requestListRequest";
                    request.Params.Add("id", "123456789");
                    request.Params.Add("fps", Performance.report.frameRate);
                    request.Params.Add("online", $"{BasePlayer.activePlayerList.Count}/{ConVar.Server.maxplayers}");

                    SendRequest(request, result =>
                    {
                        try
                        {
                            var requestIn = JsonConvert.DeserializeObject<RequestIn>(result);
                            requestIn.ExecuteRequest();
                        }
                        catch
                        {
                            // IGNORE
                        }
                    });
                } 

                public static void SendMessage(Message message)
                {
                    RequestOut request = new RequestOut();

                    request.Type   = "out_sendMessageRequest";
                    request.Params = new Dictionary<string, object>();
                    
                    request.Params.Add("id", "123456789");
                    request.Params.Add("message", message);

                    SendRequest(request, null);
                }

                public static void SendUserMuted(string requestId, string targetId, int time, string reason = "")
                {
                    RequestOut request = new RequestOut();

                    request.Type   = "out_userMuteAnswer";
                    request.Params = new Dictionary<string, object>();

                    var mutePlayer = BasePlayer.Find(targetId);

                    request.Params.Add("id", requestId);
                    request.Params.Add("result", mutePlayer != null);

                    if (mutePlayer != null) _.MutePlayer(mutePlayer, "Модератор чата", time, reason);
                    SendRequest(request, null);
                }

                public static void SendUserInfo(string requestId, string targetId)
                {
                    RequestOut request = new RequestOut();

                    request.Type   = "out_userInfoAnswer";
                    request.Params = new Dictionary<string, object>();

                    ulong parsedId = ulong.Parse(targetId);
                    
                    request.Params.Add("id", requestId);
                    request.Params.Add("info", Handler.ContainsKey(parsedId) ? Handler[parsedId] : null); 
                    request.Params.Add("messages", FindMessage(targetId));

                    SendRequest(request, null);
                }

                public static void SendLogs(string requestId, int amount)
                {
                    RequestOut request = new RequestOut();

                    request.Type   = "out_chatHistoryAnswer";
                    request.Params = new Dictionary<string, object>();

                    var listMessages = MessagesLogs.Take(amount);
                    
                    request.Params.Add("id", requestId);
                    request.Params.Add("messages", listMessages);

                    SendRequest(request, null);
                }

                public static void SendMessageGlobal(string requestId)
                {
                    RequestOut request = new RequestOut();

                    request.Type   = "out_sendMessageAnswer";
                    request.Params = new Dictionary<string, object>();

                    request.Params.Add("id", requestId);
                    request.Params.Add("result", true);

                    SendRequest(request, null);
                }
            }

            public static void SendRequest(RequestOut @out, Action<string> callback = null)
            {
                LogInfo($"Создание запроса: {@out.Type} [{WSettings.WebChatURI}]");
                _.webrequest.Enqueue(WSettings.WebChatURI + "/api/request", JsonConvert.SerializeObject(@out), (i, s) =>
                {
                    LogInfo($"Полученный результат: '{s}'"); 
                    
                    callback?.Invoke(s);
                }, _, RequestMethod.POST, new Dictionary<string, string> {["Content-Type"] = "application/json"});
            }
        }

        public class Message
        {
            [JsonProperty("displayName")] public string DisplayName;
            [JsonProperty("userId")]      public string UserID;

            [JsonProperty("targetDisplayName")] public string TargetDisplayName;
            [JsonProperty("targetUserId")]      public string TargetUserId;

            [JsonProperty("text")]   public string Text;
            [JsonProperty("isTeam")] public bool   IsTeam;
            [JsonProperty("time")]   public string Time;
        }

        #endregion

        #region Variables

        [PluginReference] private Plugin ImageLibrary;

        private static WChat _;

        private static Configuration Settings   = Configuration.Generate();

        private static Hash<ulong, PlayerInfo> Handler    = new Hash<ulong, PlayerInfo>();

        private static HashSet<Message> MessagesLogs = new HashSet<Message>();

        #endregion

        #region Initialization

        private void OnServerInitialized()
        {
            _ = this;

            RegisterPermissions();

            if (!Interface.Oxide.DataFileSystem.ExistsDatafile($"WChat/Settings") || !Interface.Oxide.DataFileSystem.ExistsDatafile($"WChat/Logs"))
            {
                SaveData();
            }
            else
            {
                Handler = Interface.Oxide.DataFileSystem.ReadObject<Hash<ulong, PlayerInfo>>($"WChat/Settings");
                MessagesLogs = Interface.Oxide.DataFileSystem.ReadObject<HashSet<Message>>($"WChat/Logs");

                if (MessagesLogs.Count > 1000)
                {
                    MessagesLogs.Clear();
                    PrintError("Data messages was cleared!"); 
                }
            }

            BasePlayer.activePlayerList.ToList().ForEach(OnPlayerConnected);
            ImageLibrary.Call("AddImage", "https://i.imgur.com/9E2Uw5O.png", "CustomLocker");

            WSettings.Validate();

            timer.Every(60, SaveData);
        }

        private void SaveData()
        {
            Interface.Oxide.DataFileSystem.WriteObject($"WChat/Settings", Handler);
            Interface.Oxide.DataFileSystem.WriteObject($"WChat/Logs", MessagesLogs);
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                Settings = Config.ReadObject<Configuration>();
            }
            catch
            {
                PrintWarning($"Error reading config, creating one new config!");
                LoadDefaultConfig();
            }

            SaveConfig();
        }

        protected override void LoadDefaultConfig() => Settings = Configuration.Generate();
        protected override void SaveConfig() => Config.WriteObject(Settings);

        #endregion

        #region Hooks

        private void OnPlayerConnected(BasePlayer player)
        {
            if (!Handler.ContainsKey(player.userID))
                Handler.Add(player.userID, PlayerInfo.Generate(player));
        }

        private string Censure(string message)
        {
            foreach (var mat in Settings.Censures)
            {
                if (message.ToLower().Contains(mat.Key))
                {
                    bool shouldReplace = true;
                    foreach (var ist in mat.Value)
                    {
                        if (message.Contains(ist))
                        {
                            shouldReplace = false;
                            break;
                        }
                    }

                    if (shouldReplace) message = message.Replace(mat.Key, "***", StringComparison.CurrentCultureIgnoreCase);
                }
            }

            return message;
        }

        private object OnPlayerChat(BasePlayer player, string message, ConVar.Chat.ChatChannel channel)
        {
            if (channel == ConVar.Chat.ChatChannel.Global)
            {
                if (!Handler.ContainsKey(player.userID)) return false;
                var playerInfo = Handler[player.userID];
                if (playerInfo.IsMuted())
                {
                    player.ChatMessage($"У вас блокировка чата, осталось: <color=orange>{TimeSpan.FromSeconds(playerInfo.UnMuteTime - Time()).ToShortString()}</color>");
                    return false; 
                }
                
                if (playerInfo.SpamTime > Time() && !player.IsAdmin && !IsModerator(player))
                {
                    player.ChatMessage($"Вы не можете писать чаще, чем раз в <color=orange>{Settings.AntiSpamInterval} секунд</color>!");
                    return false;
                }

                string realMessage    = PrepareMessage(player, message);
                string censureMessage = PrepareMessage(player, Censure(message));

                playerInfo.SpamTime = Time() + Settings.AntiSpamInterval;
                foreach (var target in BasePlayer.activePlayerList.ToList())
                {
                    if (!Handler.ContainsKey(target.userID)) return false;
                    var targetInfo = Handler[target.userID];
                    
                    if (targetInfo.Ignores.Contains(player.userID))
                        continue;

                    string prepareMessage = targetInfo.Censore ? censureMessage : realMessage;
                    SafeMessage(target, prepareMessage, Handler[player.userID].Hide ? 0UL : player.userID);
                }

                DebugEx.Log((object) ("[CHAT] " + player.displayName + $" [{player.UserIDString}]: " + message));
                AddMessage(player, message);
                return false;
            }
            else
            {
                foreach (var targetId in player.Team.members)
                {
                    if (!Handler.ContainsKey(targetId)) return false;
                    var targetInfo = Handler[targetId];
                    
                    if (targetInfo.Ignores.Contains(player.userID))
                        continue;

                    var target = BasePlayer.FindByID(targetId);
                    if (target == null || !target.IsConnected) continue;

                    string prepareMessage = PrepareMessage(player, message, true);
                    target.SendConsoleCommand("chat.add", 0, player.userID, prepareMessage);
                    target.SendConsoleCommand($"echo [<color=white>TEAM</color>] {prepareMessage}");
                }

                DebugEx.Log((object) ("[CHAT] " + player.displayName + $" [{player.UserIDString}]: " + "TEAM> " + message));
                AddMessage(player, message, true);
                return false;
            }
        }

        private string PrepareMessage(BasePlayer player, string message, bool team = false)
        {
            var settings = Handler[player.userID];

            string prefixPrepare = settings.Hook.CValue.Replace("%PREFFIX%", $"<color={settings.PrefixColor.CValue}>{settings.Prefix.CValue}</color>");

            if (settings.Prefix.CValue == string.Empty)
                prefixPrepare = "";

            string format =
                $"{(settings.Hide ? "" : prefixPrepare + " ")}<color={settings.NameColor.CValue}>{(settings.Hide ? "Модератор чата" : player.displayName)}</color>: <color={settings.MessageColor.CValue}>{message}</color>";

            if (team) format = "<color=#ADFF2F>[TEAM]</color> " + format;

            return format;
        }

        #endregion

        #region Commands

        [ConsoleCommand("UI_WChat_Handler")]
        private void CmdConsoleHandler(ConsoleSystem.Arg args)
        {
            var player = args.Player();
            if (player == null || !args.HasArgs(1)) return;

            switch (args.Args[0].ToLower())
            {
                case "mute":
                {
                    int hours = int.Parse(args.Args[2]);
                    if (hours < 0)
                    {
                        hours = 0;
                    }

                    int minutes = int.Parse(args.Args[3]);
                    if (minutes < 0)
                    {
                        minutes = 59;
                        hours--;
                    }
                    else if (minutes >= 60)
                    {
                        hours++;
                        minutes = 0;
                    }

                    int seconds = int.Parse(args.Args[4]);
                    if (seconds < 0)
                    {
                        seconds = 59;
                        minutes--;
                    }
                    else if (seconds >= 60)
                    {
                        minutes++;
                        seconds = 0;
                    }

                    int reasonId = int.Parse(args.Args[5]);
                    if (reasonId > Settings.MuteSettings.Reasons.Count - 1) return;

                    if (args.HasArgs(7))
                    {
                        MutePlayer(BasePlayer.FindByID(ulong.Parse(args.Args[1])), player.displayName, seconds + minutes * 60 + hours * 60 * 60, Settings.MuteSettings.Reasons.ElementAtOrDefault(reasonId));
                        CuiHelper.DestroyUi(player, Layer);
                        return;
                    }

                    InitializeMuteModerator(player, ulong.Parse(args.Args[1]), hours, minutes, seconds, reasonId);
                    break;
                }
                case "testmessage":
                {
                    player.SendConsoleCommand("chat.add", 0, Handler[player.userID].Hide ? 0UL : player.userID, PrepareMessage(player, "Тестовое сообщение в чат!"));

                    if (!Handler[player.userID].Hide)
                    {
                        player.SendConsoleCommand("chat.add", 0, player.userID, PrepareMessage(player, "Тестовое сообщение в чат!", true));
                    }

                    InitializeMute(player, true);
                    break;
                }
                case "mutetimeupdate":
                {
                    InitializeMute(player, true);
                    break;
                }
                case "unignore":
                {
                    if (!args.HasArgs(2)) return;
                    ulong id = ulong.Parse(args.Args[1]);

                    if (!Handler[player.userID].Ignores.Contains(id)) return;
                    Handler[player.userID].Ignores.Remove(id);

                    InitializeIgnore(player);
                    break;
                }
                case "set":
                {
                    if (!args.HasArgs(2)) return;
                    int index = int.Parse(args.HasArgs(3) ? args.Args[2] : "0"); // Да, не всё совершенно

                    switch (args.Args[1].ToLower())
                    {
                        case "censore":
                        {
                            Handler[player.userID].Censore = !Handler[player.userID].Censore;
                            InitializeButtons(player);
                            break;
                        }
                        case "sounds":
                        {
                            Handler[player.userID].SoundPm = !Handler[player.userID].SoundPm;
                            InitializeButtons(player);
                            break;
                        }
                        case "tips":
                        {
                            Handler[player.userID].Tips = !Handler[player.userID].Tips;
                            InitializeButtons(player);
                            break;
                        }
                        case "hook":
                        {
                            var list         = Settings.PrefixSettings.Hooks.Where(p => p.CanUse(player));
                            var currentIndex = list.ToList().IndexOf(Handler[player.userID].Hook);

                            var newHook                  = list.ElementAtOrDefault(currentIndex + 1);
                            if (newHook == null) newHook = list.ElementAtOrDefault(0);

                            Handler[player.userID].Hook = newHook;
                            InitializePrefix(player);
                            break;
                        }
                        case "prefix":
                        {
                            var prefix = Settings.PrefixSettings.Prefixes.ElementAtOrDefault(index);
                            if (prefix == null || !prefix.CanUse(player)) return;

                            Handler[player.userID].Prefix = prefix;
                            InitializeInterface(player, "prefix");
                            break;
                        }
                        case "prefixcolor":
                        {
                            var color = Settings.PrefixSettings.Colors.ElementAtOrDefault(index);
                            if (color == null || !color.CanUse(player)) return;

                            Handler[player.userID].PrefixColor = color;
                            InitializeInterface(player, "prefix");
                            break;
                        }
                        case "namecolor":
                        {
                            var color = Settings.NameSettings.Colors.ElementAtOrDefault(index);
                            if (color == null || !color.CanUse(player)) return;

                            Handler[player.userID].NameColor = color;
                            InitializeInterface(player, "name");
                            break;
                        }
                        case "messagecolor":
                        {
                            var color = Settings.MessageSettings.Colors.ElementAtOrDefault(index);
                            if (color == null || !color.CanUse(player)) return;

                            Handler[player.userID].MessageColor = color;
                            InitializeInterface(player, "message");
                            break;
                        }
                        case "hide":
                        {
                            var hide = index != 0;
                            if (!IsModerator(player)) return;

                            Handler[player.userID].Hide = hide;
                            if (Handler[player.userID].Hide)
                                CuiHelper.DestroyUi(player, Layer + ".Prefix");
                            else InitializePrefix(player);

                            InitializeInterface(player, "name");
                            break;
                        }
                    }

                    break;
                }
                case "open":
                {
                    if (!args.HasArgs(2)) return;
                    string element = args.Args[1];

                    InitializeInterface(player, element);
                    break;
                }
            }
        }

        [ChatCommand("chat")]
        private void CmdChatInterface(BasePlayer player, string command, string[] args) => InitializeInterface(player);

        [ChatCommand("chat.mute")]
        private void CmdChatMute(BasePlayer player, string command, string[] args)
        {
            if (!IsModerator(player)) return;

            if (args.Length == 0)
            {
                player.ChatMessage($"Используйте команду правильно: /chat.mute <name/id>");
                return;
            }

            var findTarget = BasePlayer.activePlayerList.FirstOrDefault(p => p.displayName.ToLower().Contains(args[0].ToLower()) || p.UserIDString == args[0]);
            if (findTarget == null || !findTarget.IsConnected)
            {
                player.ChatMessage("Игрок не найден!");
                return;
            }

            InitializeMuteModerator(player, findTarget.userID, Settings.MuteSettings.DefaultHours, Settings.MuteSettings.DefaultMinutes, Settings.MuteSettings.DefaultSeconds, 0);
        }

        [ChatCommand("chat.find")]
        private void CmdChatFind(BasePlayer player, string command, string[] args)
        {
            if (!IsModerator(player)) return;

            if (args.Length == 0)
            {
                player.ChatMessage($"Используйте команду правильно: /chat.find <name/id/message>");
                return;
            }

            var findTarget = args[0];
            var @out       = FindMessage(findTarget);

            player.SendConsoleCommand($"echo Поиск сообщений по фильтру: '{findTarget}'");
            foreach (var check in @out)
                player.SendConsoleCommand($"echo [{check.Time}] {check.DisplayName}: {check.Text}");

            player.ChatMessage($"Найденные сообщения экспортированы в консоль <color=orange>F1</color>");
        }

        #endregion

        #region Interface

        private static string Layer = "UI_WChat_Interface";

        private static void InitializeMuteModerator(BasePlayer player, ulong userId, int hour, int minutes, int seconds, int reasonId)
        {
            if (!IsModerator(player)) return;

            CuiElementContainer container = new CuiElementContainer();
            CuiHelper.DestroyUi(player, Layer);

            container.Add(new CuiPanel
            {
                CursorEnabled = true,
                RectTransform = {AnchorMin = "0 0", AnchorMax                   = "1 1", OffsetMax = "0 0"},
                Image         = {Color     = "0.117 0.121 0.109 0.95", Material = ""}
            }, "Overlay", Layer);

            container.Add(new CuiPanel
            {
                RectTransform = {AnchorMin = "0 0", AnchorMax = "1 1", OffsetMin = "0 0", OffsetMax = "0 0"},
                Image         = {Color     = "0 0 0 0"}
            }, Layer, Layer + ".BG");

            float verticalMargin = Settings.MuteSettings.Reasons.Count / 2f * 32 + (Settings.MuteSettings.Reasons.Count - 1) / 2f * 5;

            container.Add(new CuiLabel
            {
                RectTransform =
                {
                    AnchorMin = "0 0.5", AnchorMax = "1 0.5", OffsetMin = $"0 {verticalMargin + 20}", OffsetMax = $"0 {70 + verticalMargin}"
                },
                Text = {Text = $"БЛОКИРОВКА ЧАТА ИГРОКУ {(_.permission.GetUserData(userId.ToString()).LastSeenNickname).ToUpper()}", Align = TextAnchor.UpperCenter, Font = "robotocondensed-bold.ttf", FontSize = 28, Color = "1 1 1 0.8"}
            }, Layer + ".BG");

            container.Add(new CuiButton
            {
                RectTransform = {AnchorMin = "1 0.8", AnchorMax = "1 1", OffsetMin = "-150 0", OffsetMax = "-25 -35"},
                Button        = {Color     = "0 0 0 0", Close   = Layer},
                Text          = {Text      = "ВЫХОД", Align     = TextAnchor.UpperCenter, Font = "robotocondensed-bold.ttf", FontSize = 28, Color = "1 1 1 0.5"}
            }, Layer + ".BG");

            var targetInfo = Handler[userId];
            if (!targetInfo.IsMuted())
            {
                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0.5 0.5", AnchorMax = "0.5 0.5", OffsetMin = $"-35 {-16 + verticalMargin}", OffsetMax = $"35 {16 + verticalMargin}"},
                    Image         = {Color     = "1 1 1 0.15"}
                }, Layer + ".BG", Layer + ".BG.Minutes");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 1", AnchorMax = "1 1", OffsetMin             = "0 3", OffsetMax                        = "0 35"},
                    Text          = {Text      = "МИНУТ", Align   = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 14, Color = "1 1 1 0.5"}
                }, Layer + ".BG.Minutes");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax = "0 1", OffsetMin              = "-64 0", OffsetMax                   = "-20 2"},
                    Text          = {Text      = ":", Align       = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 24, Color = "1 1 1 0.8"}
                }, Layer + ".BG.Minutes");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "1 0", AnchorMax = "1 1", OffsetMin              = "20 0", OffsetMax                    = "64 2"},
                    Text          = {Text      = ":", Align       = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 24, Color = "1 1 1 0.8"}
                }, Layer + ".BG.Minutes");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax          = "1 1", OffsetMax              = "0 0"},
                    Text          = {Text      = minutes.ToString(), Align = TextAnchor.MiddleCenter, Font = "robotocondensed-regular.ttf", FontSize = 24}
                }, Layer + ".BG.Minutes");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax     = "0 1", OffsetMin = "-32 0", OffsetMax = "0 0"},
                    Button        = {Color     = "1 1 1 0.4", Command = $"UI_WChat_Handler mute {userId} {hour} {minutes - 1} {seconds} {reasonId}"},
                    Text          = {Text      = "-", Align           = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 24}
                }, Layer + ".BG.Minutes");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "1 0", AnchorMax     = "1 1", OffsetMin = "0 0", OffsetMax = "32 0"},
                    Button        = {Color     = "1 1 1 0.4", Command = $"UI_WChat_Handler mute {userId} {hour} {minutes + 1} {seconds} {reasonId}"},
                    Text          = {Text      = "+", Align           = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 24}
                }, Layer + ".BG.Minutes");

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0.5 0.5", AnchorMax = "0.5 0.5", OffsetMin = $"-190 {-16 + verticalMargin}", OffsetMax = $"-120 {16 + verticalMargin}"},
                    Image         = {Color     = "1 1 1 0.15"}
                }, Layer + ".BG", Layer + ".BG.Hours");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 1", AnchorMax = "1 1", OffsetMin             = "0 3", OffsetMax                        = "0 35"},
                    Text          = {Text      = "ЧАСОВ", Align   = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 14, Color = "1 1 1 0.5"}
                }, Layer + ".BG.Hours");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax       = "1 1", OffsetMax              = "0 0"},
                    Text          = {Text      = hour.ToString(), Align = TextAnchor.MiddleCenter, Font = "robotocondensed-regular.ttf", FontSize = 24}
                }, Layer + ".BG.Hours");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax     = "0 1", OffsetMin = "-32 0", OffsetMax = "0 0"},
                    Button        = {Color     = "1 1 1 0.4", Command = $"UI_WChat_Handler mute {userId} {hour - 1} {minutes} {seconds} {reasonId}"},
                    Text          = {Text      = "-", Align           = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 24}
                }, Layer + ".BG.Hours");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "1 0", AnchorMax     = "1 1", OffsetMin = "0 0", OffsetMax = "32 0"},
                    Button        = {Color     = "1 1 1 0.4", Command = $"UI_WChat_Handler mute {userId} {hour + 1} {minutes} {seconds} {reasonId}"},
                    Text          = {Text      = "+", Align           = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 24}
                }, Layer + ".BG.Hours");

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0.5 0.5", AnchorMax = "0.5 0.5", OffsetMin = $"120 {-16 + verticalMargin}", OffsetMax = $"190 {16 + verticalMargin}"},
                    Image         = {Color     = "1 1 1 0.15"}
                }, Layer + ".BG", Layer + ".BG.Seconds");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 1", AnchorMax = "1 1", OffsetMin             = "0 3", OffsetMax                        = "0 35"},
                    Text          = {Text      = "СЕКУНД", Align  = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 14, Color = "1 1 1 0.5"}
                }, Layer + ".BG.Seconds");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax          = "1 1", OffsetMax              = "0 0"},
                    Text          = {Text      = seconds.ToString(), Align = TextAnchor.MiddleCenter, Font = "robotocondensed-regular.ttf", FontSize = 24}
                }, Layer + ".BG.Seconds");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax     = "0 1", OffsetMin = "-32 0", OffsetMax = "0 0"},
                    Button        = {Color     = "1 1 1 0.4", Command = $"UI_WChat_Handler mute {userId} {hour} {minutes} {seconds - 1} {reasonId}"},
                    Text          = {Text      = "-", Align           = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 24}
                }, Layer + ".BG.Seconds");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "1 0", AnchorMax     = "1 1", OffsetMin = "0 0", OffsetMax = "32 0"},
                    Button        = {Color     = "1 1 1 0.4", Command = $"UI_WChat_Handler mute {userId} {hour} {minutes} {seconds + 1} {reasonId}"},
                    Text          = {Text      = "+", Align           = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 24}
                }, Layer + ".BG.Seconds");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0.5 0.5", AnchorMax        = "0.5 0.5", OffsetMin         = $"-222 {verticalMargin - 40}", OffsetMax = $"222 {verticalMargin - 20}"},
                    Text          = {Text      = "ПРИЧИНА БЛОКИРОВКИ", Align = TextAnchor.UpperCenter, Font = "robotocondensed-regular.ttf", FontSize  = 14, Color = "1 1 1 0.5"}
                }, Layer + ".BG");

                foreach (var check in Settings.MuteSettings.Reasons)
                {
                    container.Add(new CuiPanel
                    {
                        RectTransform = {AnchorMin = "0.5 0.5", AnchorMax = "0.5 0.5", OffsetMin = $"-222 {verticalMargin - 72}", OffsetMax = $"222 {verticalMargin - 40}"},
                        Image         = {Color     = "1 1 1 0.2"}
                    }, Layer + ".BG", Layer + ".BG.Reason" + verticalMargin);

                    container.Add(new CuiButton
                    {
                        RectTransform = {AnchorMin = "0 0", AnchorMax = "0 1", OffsetMin = $"5 5", OffsetMax = $"27 -5"},
                        Button =
                        {
                            Color = reasonId == Settings.MuteSettings.Reasons.IndexOf(check) ? "1 1 1 0.4" : "1 1 1 0.1", Command = $"UI_WChat_Handler mute {userId} {hour} {minutes} {seconds} {Settings.MuteSettings.Reasons.IndexOf(check)}"
                        },
                        Text = {Text = ""}
                    }, Layer + ".BG.Reason" + verticalMargin);

                    container.Add(new CuiLabel
                    {
                        RectTransform = {AnchorMin = "0 0", AnchorMax      = "1 1", OffsetMin                     = "32 0", OffsetMax     = "0 0"},
                        Text          = {Text      = check.ToUpper(), Font = "robotocondensed-regular.ttf", Color = "1 1 1 0.5", FontSize = 16, Align = TextAnchor.MiddleLeft}
                    }, Layer + ".BG.Reason" + verticalMargin);

                    verticalMargin -= 35;
                }

                verticalMargin -= 45;
                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "0.5 0.5", AnchorMax = "0.5 0.5", OffsetMin = $"-100 {verticalMargin - 28}", OffsetMax = $"100 {verticalMargin}"},
                    Button        = {Color     = "1 1 1 0.4", Command = $"UI_WChat_Handler mute {userId} {hour} {minutes} {seconds} {reasonId} mute"},
                    Text          = {Text      = "ОТПРАВИТЬ", Align   = TextAnchor.MiddleCenter, Font = "robotocondensed-regular.ttf", FontSize = 14}
                }, Layer + ".BG"); 
            }
            else
            {
                container.Add(new CuiLabel
                {
                    RectTransform = { AnchorMin = "0 0.5", AnchorMax = "1 0.5", OffsetMin = $"0 {verticalMargin - 10}", OffsetMax = $"0 {40 + verticalMargin}" },
                    Text = {Text = $"ЧАТ ЗАБЛОКИРОВАН НА <b>{TimeSpan.FromSeconds(targetInfo.UnMuteTime - Time()).ToShortString()}</b>", Align = TextAnchor.UpperCenter, Font = "robotocondensed-regular.ttf", FontSize = 20, Color = "1 1 1 0.6"}
                }, Layer + ".BG");
                
                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "0.5 0.5", AnchorMax = "0.5 0.5", OffsetMin = $"-100 {verticalMargin - 28}", OffsetMax = $"100 {verticalMargin}"},
                    Button        = {Color     = "1 1 1 0.4", Command = $"UI_WChat_Handler mute {userId} {0} {0} {0} {reasonId} mute"}, 
                    Text          = {Text      = "РАЗБЛОКИРОВАТЬ", Align   = TextAnchor.MiddleCenter, Font = "robotocondensed-regular.ttf", FontSize = 14}
                }, Layer + ".BG"); 
            }

            


            CuiHelper.AddUi(player, container);
        }

        private static void InitializeInterface(BasePlayer player, string updateLayer = "")
        {
            CuiElementContainer container = new CuiElementContainer();

            if (updateLayer == string.Empty)
            {
                CuiHelper.DestroyUi(player, Layer);

                container.Add(new CuiPanel
                {
                    CursorEnabled = true,
                    RectTransform = {AnchorMin = "0 0", AnchorMax                   = "1 1", OffsetMax = "0 0"},
                    Image         = {Color     = "0.117 0.121 0.109 0.95", Material = ""}
                }, "Overlay", Layer);

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax = "1 1", OffsetMin = "0 0", OffsetMax = "0 0"},
                    Image         = {Color     = "0 0 0 0"}
                }, Layer, Layer + ".BG");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0.8", AnchorMax               = "1 1", OffsetMax             = "0 -35"},
                    Text          = {Text      = "НАСТРОЙКИ ИГРОВОГО ЧАТА", Align = TextAnchor.UpperCenter, Font = "robotocondensed-bold.ttf", FontSize = 28, Color = "1 1 1 0.8"}
                }, Layer + ".BG");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "1 0.8", AnchorMax = "1 1", OffsetMin = "-150 0", OffsetMax = "-25 -35"},
                    Button        = {Color     = "0 0 0 0", Close   = Layer},
                    Text          = {Text      = "ВЫХОД", Align     = TextAnchor.UpperCenter, Font = "robotocondensed-bold.ttf", FontSize = 28, Color = "1 1 1 0.5"}
                }, Layer + ".BG");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "0 0.8", AnchorMax = "0 1", OffsetMin = "25 0", OffsetMax = "350 -35"},
                    Button        = {Color     = "0 0 0 0", Command = "UI_WChat_Handler testmessage"},
                    Text          = {Text      = "ПРОВЕРИТЬ", Align = TextAnchor.UpperLeft, Font = "robotocondensed-bold.ttf", FontSize = 28, Color = "1 1 1 0.5"}
                }, Layer + ".BG");
            }

            CuiHelper.AddUi(player, container);

            switch (updateLayer)
            {
                case "prefix":
                {
                    InitializePrefix(player, true);
                    break;
                }
                case "name":
                {
                    InitializeName(player, true);
                    break;
                }
                case "message":
                {
                    InitializeMessage(player, true);
                    break;
                }
                case "buttons":
                {
                    InitializeButtons(player);
                    break;
                }
                case "ignore":
                {
                    InitializeIgnore(player);
                    break;
                }
                default:
                {
                    InitializePrefix(player);
                    InitializeName(player);
                    InitializeMessage(player);
                    InitializeButtons(player);
                    InitializeIgnore(player);
                    break;
                }
            }

            InitializeMute(player);
        }

        private static void InitializeMute(BasePlayer player, bool update = false)
        {
            CuiElementContainer container      = new CuiElementContainer();
            var                 playerSettings = Handler[player.userID];

            /*if (!playerSettings.IsMuted())
            { 
                CuiHelper.DestroyUi(player, Layer + ".Mute");
                return;
            }*/

            if (!update)
            {
                CuiHelper.DestroyUi(player, Layer + ".Mute");

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "1 0", AnchorMax                                                       = "1 0", OffsetMin = "-260 10", OffsetMax = "-10 65"},
                    Image         = {Color     = playerSettings.IsMuted() ? "0.6 0.3 0.3 1" : "0.3 0.3 0.3 1", Material = ""}
                }, Layer, Layer + ".Mute");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax         = "1 1", OffsetMin           = "30 5", OffsetMax                    = "0 -5"},
                    Text          = {Text      = "БЛОКИРОВКА ЧАТА", Align = TextAnchor.UpperLeft, Font = "robotocondensed-bold.ttf", FontSize = 24, Color = "1 1 1 0.8"}
                }, Layer + ".Mute");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax = "1 1", OffsetMin            = "8 -15", OffsetMax                   = "0 15"},
                    Text          = {Text      = "!", Align       = TextAnchor.MiddleLeft, Font = "robotocondensed-bold.ttf", FontSize = 50, Color = playerSettings.IsMuted() ? "1 1 1 1" : "1 1 1 0.05"}
                }, Layer + ".Mute");

                CuiHelper.AddUi(player, container);
                InitializeMute(player, true);
                return;
            }
            else
            {
                CuiHelper.DestroyUi(player, Layer + ".Mute.Update");

                TimeSpan unMuteTime;
                string   result;

                if (playerSettings.UnMuteTime > Time())
                {
                    unMuteTime = TimeSpan.FromSeconds(playerSettings.UnMuteTime - Time());
                    result     = $"{(unMuteTime.Hours > 0 ? $"{unMuteTime.Hours} ЧАС." : "")} {unMuteTime.Minutes} МИН. {unMuteTime.Seconds} СЕК.";
                }
                else
                {
                    result = "ОТСУТСТВУЕТ";
                }


                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax = "1 1", OffsetMin           = "30 5", OffsetMax                       = "0 0"},
                    Text          = {Text      = result, Align    = TextAnchor.LowerLeft, Font = "robotocondensed-regular.ttf", FontSize = 18, Color = "1 1 1 0.6"}
                }, Layer + ".Mute", Layer + ".Mute.Update");
            }

            container.Add(new CuiElement
            {
                Parent = Layer,
                Components =
                {
                    new CuiCountdownComponent {Command       = "UI_WChat_Handler muteTimeUpdate", EndTime = 0, StartTime     = 1, Step = 1},
                    new CuiRectTransformComponent {AnchorMin = "0 0", AnchorMax                           = "1 0", OffsetMin = "0 -50"}
                }
            });

            CuiHelper.AddUi(player, container);
        }

        private static void InitializeIgnore(BasePlayer player)
        {
            CuiElementContainer container      = new CuiElementContainer();
            var                 playerSettings = Handler[player.userID];

            CuiHelper.DestroyUi(player, Layer + ".Ignore");
            container.Add(new CuiPanel
            {
                RectTransform = {AnchorMin = "1 0", AnchorMax = "1 0", OffsetMin = "-260 10", OffsetMax = "-10 100"},
                Image         = {Color     = "0 0 0 0"}
            }, Layer + ".BG", Layer + ".Ignore");

            float verticalMargin = 25;
            foreach (var check in playerSettings.Ignores.Where(p => _.permission.GetUserData(p.ToString())?.LastSeenNickname.ToUpper() != "UNNAMED"))
            {
                verticalMargin += 35;

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax = "1 0", OffsetMin = $"0 {verticalMargin}", OffsetMax = $"0 {verticalMargin + 32}"},
                    Image         = {Color     = "1 1 1 0.15"}
                }, Layer + ".Ignore", Layer + $".Ignore.{verticalMargin}");

                container.Add(new CuiButton
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax        = "0 1", OffsetMin                  = "6 6", OffsetMax = "26 -6"},
                    Button        = {Color     = "0.8 0.5 0.5 1", Sprite = "assets/icons/close.png", Command = $"UI_WChat_Handler unignore {check}"},
                    Text          = {Text      = " "}
                }, Layer + $".Ignore.{verticalMargin}");

                container.Add(new CuiLabel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax                                                                           = "1 1", OffsetMin  = "35 0", OffsetMax                    = "0 0"},
                    Text          = {Text      = _.permission.GetUserData(check.ToString())?.LastSeenNickname.ToUpper() ?? "UNKNOWN", Color = "1 1 1 0.7", Font = "robotocondensed-bold.ttf", FontSize = 18, Align = TextAnchor.MiddleLeft}
                }, Layer + $".Ignore.{verticalMargin}");
            }

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 1", AnchorMax                    = "1 1", OffsetMin                        = "0 3", OffsetMax = "0 50"},
                Text          = {Text      = "СПИСОК ИГНОРИРУЕМЫХ ИГРОКОВ", Font = "robotocondensed-regular.ttf", FontSize = 14, Align        = TextAnchor.LowerCenter, Color = "1 1 1 0.7"}
            }, Layer + $".Ignore.{verticalMargin}");

            CuiHelper.AddUi(player, container);
        }

        private static void InitializeButtons(BasePlayer player)
        {
            CuiElementContainer container      = new CuiElementContainer();
            var                 playerSettings = Handler[player.userID];

            CuiHelper.DestroyUi(player, Layer + ".Buttons");
            container.Add(new CuiPanel
            {
                RectTransform = {AnchorMin = "0 0", AnchorMax = "0 0", OffsetMin = "10 10", OffsetMax = "500 100"},
                Image         = {Color     = "0 0 0 0"}
            }, Layer + ".BG", Layer + ".Buttons");

            container.Add(new CuiButton
            {
                RectTransform = {AnchorMin = "0 0", AnchorMax                               = "0.33 0", OffsetMin = "0 0", OffsetMax = "-2 30"},
                Button        = {Color     = "1 1 1 0.15", Command                          = $"UI_WChat_Handler set censore"},
                Text          = {Text      = playerSettings.Censore ? "ВКЛ" : "ВЫКЛ", Align = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 18, Color = "1 1 1 0.5"}
            }, Layer + ".Buttons", Layer + ".Buttons.Censure");

            container.Add(new CuiPanel
            {
                RectTransform = {AnchorMin = "1 0", AnchorMax = "1 1", OffsetMin = "-20 5", OffsetMax = "-5 -5"},
                Image         = {Color     = playerSettings.Censore ? "0.6 0.9 0.6 1" : "0.9 0.6 0.6 1"}
            }, Layer + ".Buttons.Censure");

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 1", AnchorMax = "1 1", OffsetMin             = "0 5", OffsetMax                        = "0 250"},
                Text          = {Text      = "ЦЕНЗУРА", Align = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 14, Color = "1 1 1 0.5"}
            }, Layer + ".Buttons.Censure");

            container.Add(new CuiButton
            {
                RectTransform = {AnchorMin = "0.33 0", AnchorMax                         = "0.66 0", OffsetMin = "2 0", OffsetMax = "-2 30"},
                Button        = {Color     = "1 1 1 0.15", Command                       = $"UI_WChat_Handler set tips"},
                Text          = {Text      = playerSettings.Tips ? "ВКЛ" : "ВЫКЛ", Align = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 18, Color = "1 1 1 0.5"}
            }, Layer + ".Buttons", Layer + ".Buttons.Help");

            container.Add(new CuiPanel
            {
                RectTransform = {AnchorMin = "1 0", AnchorMax = "1 1", OffsetMin = "-20 5", OffsetMax = "-5 -5"},
                Image         = {Color     = playerSettings.Tips ? "0.6 0.9 0.6 1" : "0.9 0.6 0.6 1"}
            }, Layer + ".Buttons.Help");

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 1", AnchorMax   = "1 1", OffsetMin             = "0 5", OffsetMax                        = "0 250"},
                Text          = {Text      = "ПОДСКАЗКИ", Align = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 14, Color = "1 1 1 0.5"}
            }, Layer + ".Buttons.Help");

            container.Add(new CuiButton
            {
                RectTransform = {AnchorMin = "0.66 0", AnchorMax                            = "0.99 0", OffsetMin = "2 0", OffsetMax = "0 30"},
                Button        = {Color     = "1 1 1 0.15", Command                          = $"UI_WChat_Handler set sounds"},
                Text          = {Text      = playerSettings.SoundPm ? "ВКЛ" : "ВЫКЛ", Align = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 18, Color = "1 1 1 0.5"}
            }, Layer + ".Buttons", Layer + ".Buttons.Sound");
            container.Add(new CuiPanel
            {
                RectTransform = {AnchorMin = "1 0", AnchorMax = "1 1", OffsetMin = "-20 5", OffsetMax = "-5 -5"},
                Image         = {Color     = playerSettings.SoundPm ? "0.6 0.9 0.6 1" : "0.9 0.6 0.6 1"}
            }, Layer + ".Buttons.Sound");

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 1", AnchorMax        = "1 1", OffsetMin             = "0 5", OffsetMax                        = "0 250"},
                Text          = {Text      = "ЗВУК СООБЩЕНИЙ", Align = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 14, Color = "1 1 1 0.5"}
            }, Layer + ".Buttons.Sound");

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "1 0", AnchorMax                                                                                 = "1 1", OffsetMin            = "5 -5", OffsetMax                       = "400 5"},
                Text          = {Text      = "нажатие на превью вашего префикса, позволяет сменить тип отображения префикса".ToUpper(), Align = TextAnchor.MiddleLeft, Font = "robotocondensed-regular.ttf", FontSize = 14, Color = "1 1 1 0.2"}
            }, Layer + ".Buttons.Sound");

            CuiHelper.AddUi(player, container);
        }

        private static void InitializeMessage(BasePlayer player, bool open = true)
        {
            CuiElementContainer container      = new CuiElementContainer();
            var                 playerSettings = Handler[player.userID];

            CuiHelper.DestroyUi(player, Layer + ".Message");

            container.Add(new CuiPanel
            {
                RectTransform = {AnchorMin = "0.5 0.5", AnchorMax = "0.5 0.5", OffsetMin = $"75 200", OffsetMax = $"475 250"},
                Image         = {Color     = "0.91 0.91 0.91 0.2"}
            }, Layer + ".BG", Layer + ".Message");

            bool   isColor       = playerSettings.MessageColor.CValue.Length > 0;
            string colorTag      = isColor ? $"<color={playerSettings.MessageColor.CValue}>" : "";
            string colorTagClose = isColor ? $"</color>" : "";
            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 0", AnchorMax                                      = "1 1", OffsetMin            = "10 0", OffsetMax                    = "0 0"},
                Text          = {Text      = $"{colorTag}ТЕСТОВОЕ СООБЩЕНИЕ{colorTagClose}", Align = TextAnchor.MiddleLeft, Font = "robotocondensed-bold.ttf", FontSize = 32, Color = "1 1 1 0.75"}
            }, Layer + ".Message");

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 1", AnchorMax        = "1 1", OffsetMin             = "0 3", OffsetMax                        = "0 250"},
                Text          = {Text      = "ВАШЕ СООБЩЕНИЕ", Align = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 18, Color = "1 1 1 0.5"}
            }, Layer + ".Message");

            if (open)
            {
                float verticalMargin    = 10;
                float maxVerticalMargin = 1000;

                var orderedPrefixColorList = Settings.MessageSettings.Colors.OrderByDescending(p => p.CanUse(player));

                for (int i = 0; i < Settings.MessageSettings.Colors.Count; i++)
                {
                    var color = orderedPrefixColorList.ElementAtOrDefault(i);

                    verticalMargin -= 35;

                    if (color != null)
                    {
                        container.Add(new CuiPanel
                        {
                            RectTransform = {AnchorMin = "0.25 0", AnchorMax = "0.25 0", OffsetMin = $"1 {verticalMargin - 2}", OffsetMax = $"10 {verticalMargin}"},
                            Image         = {Color     = playerSettings.MessageColor.CValue == color.CValue ? "1 1 1 0.5" : "1 1 1 0.15"}
                        }, Layer + ".Message");

                        container.Add(new CuiPanel
                        {
                            RectTransform = {AnchorMin = "0.25 0", AnchorMax = "0.25 0", OffsetMin = $"10 {verticalMargin - 16}", OffsetMax = $"150 {verticalMargin + 16}"},
                            Image         = {Color     = HexToRustFormat(color.CValue)}
                        }, Layer + ".Message", Layer + verticalMargin + ".Message.Color");

                        if (!color.CanUse(player))
                        {
                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0 0", AnchorMax      = "1 1", OffsetMin = $"0 0", OffsetMax = $"0 0"},
                                Image         = {Color     = "0 0 0 0.9", Material = ""}
                            }, Layer + verticalMargin + ".Message.Color");

                            container.Add(new CuiElement
                            {
                                Parent = Layer + verticalMargin + ".Message.Color",
                                Components =
                                {
                                    new CuiRawImageComponent {Png            = (string) _.ImageLibrary.Call("GetImage", "CustomLocker"), Color = "1 1 1 0.6", Material = ""},
                                    new CuiRectTransformComponent {AnchorMin = "0.5 0.5", AnchorMax                                            = "0.5 0.5", OffsetMin  = "-8 -8", OffsetMax = "8 8"}
                                }
                            });
                        }

                        if (playerSettings.MessageColor.CValue == color.CValue && verticalMargin <= maxVerticalMargin)
                        {
                            maxVerticalMargin = verticalMargin;

                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0.25 0", AnchorMax = "0.25 0", OffsetMin = $"8 {verticalMargin - 16}", OffsetMax = $"10 {verticalMargin - 2}"},
                                Image         = {Color     = "1 1 1 0.5"}
                            }, Layer + ".Message");

                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0.25 0", AnchorMax = "0.25 0", OffsetMin = $"8 {verticalMargin - 18}", OffsetMax = $"150 {verticalMargin - 16}"},
                                Image         = {Color     = "1 1 1 0.5"}
                            }, Layer + ".Message");
                        }

                        container.Add(new CuiButton
                        {
                            RectTransform = {AnchorMin = "0 0", AnchorMax   = "1 1", OffsetMin = $"0 0", OffsetMax = $"0 0"},
                            Button        = {Color     = "0 0 0 0", Command = $"UI_WChat_Handler set messageColor {Settings.MessageSettings.Colors.ToList().IndexOf(color)}"},
                            Text          = {Text      = ""}
                        }, Layer + verticalMargin + ".Message.Color");
                    }
                }

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax = "1 0", OffsetMin = $"0 0", OffsetMax = "0 2"},
                    Image         = {Color     = "1 1 1 0.5"}
                }, Layer + ".Message");

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0.25 0", AnchorMax = "0.25 0", OffsetMin = $"-1 {maxVerticalMargin - 2}", OffsetMax = "1 0"},
                    Image         = {Color     = "1 1 1 0.5"}
                }, Layer + ".Message");

                if (verticalMargin < maxVerticalMargin)
                {
                    container.Add(new CuiPanel
                    {
                        RectTransform = {AnchorMin = "0.25 0", AnchorMax = "0.25 0", OffsetMin = $"-1 {verticalMargin - 2}", OffsetMax = $"1 {maxVerticalMargin}"},
                        Image         = {Color     = "1 1 1 0.15"}
                    }, Layer + ".Message");
                }
            }

            CuiHelper.AddUi(player, container);
        }

        private static void InitializePrefix(BasePlayer player, bool open = true)
        {
            CuiElementContainer container      = new CuiElementContainer();
            var                 playerSettings = Handler[player.userID];
            if (playerSettings.Hide) return;


            CuiHelper.DestroyUi(player, Layer + ".Prefix");

            container.Add(new CuiButton
            {
                RectTransform = {AnchorMin = "0.5 0.5", AnchorMax          = "0.5 0.5", OffsetMin = $"-475 200", OffsetMax = $"-225 250"},
                Button        = {Color     = "0.91 0.91 0.91 0.2", Command = "UI_WChat_Handler set hook"},
                Text          = {Text      = ""}
            }, Layer + ".BG", Layer + ".Prefix");

            bool   isColor       = playerSettings.PrefixColor.CValue.Length > 0;
            string colorTag      = isColor ? $"<color={playerSettings.PrefixColor.CValue}>" : "";
            string colorTagClose = isColor ? $"</color>" : "";
 
            string prefixPrepare = playerSettings.Hook.CValue.Replace("%PREFFIX%", $"<color={playerSettings.PrefixColor.CValue}>{playerSettings.Prefix.CValue}</color>");

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 0", AnchorMax          = "1 1", OffsetMin             = "0 0", OffsetMax                     = "-10 0"},
                Text          = {Text      = $"{prefixPrepare}", Align = TextAnchor.MiddleRight, Font = "robotocondensed-bold.ttf", FontSize = 32, Color = "1 1 1 0.75"}
            }, Layer + ".Prefix");

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 1", AnchorMax     = "1 1", OffsetMin             = "0 3", OffsetMax                        = "0 250"},
                Text          = {Text      = "ВАШ ПРЕФИКС", Align = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 18, Color = "1 1 1 0.5"}
            }, Layer + ".Prefix");

            if (open)
            {
                float verticalMargin    = 10;
                float maxVerticalMargin = 1000;

                var orderedPrefixList      = Settings.PrefixSettings.Prefixes.OrderByDescending(p => p.CanUse(player)).ToList();
                var orderedPrefixColorList = Settings.PrefixSettings.Colors.OrderByDescending(p => p.CanUse(player)).ToList();

                for (int i = 0; i < Math.Max(Settings.PrefixSettings.Prefixes.Count, Settings.PrefixSettings.Colors.Count); i++)
                {
                    var prefix = orderedPrefixList.ElementAtOrDefault(i);
                    var color  = orderedPrefixColorList.ElementAtOrDefault(i);

                    verticalMargin -= 35;

                    if (prefix != null)
                    {
                        container.Add(new CuiPanel
                        {
                            RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-10 {verticalMargin - 2}", OffsetMax = $"-1 {verticalMargin}"},
                            Image         = {Color     = playerSettings.Prefix.CValue == prefix.CValue ? "1 1 1 0.5" : "1 1 1 0.15"}
                        }, Layer + ".Prefix");

                        container.Add(new CuiPanel
                        {
                            RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-150 {verticalMargin - 16}", OffsetMax = $"-10 {verticalMargin + 16}"},
                            Image         = {Color     = !prefix.CanUse(player) ? "1 1 1 0.05" : "1 1 1 0.15"}
                        }, Layer + ".Prefix", Layer + verticalMargin + ".Prefix");

                        container.Add(new CuiLabel
                        {
                            RectTransform = {AnchorMin = "0 0", AnchorMax            = "1 1", OffsetMax             = "-5 0"},
                            Text          = {Text      = $"{(prefix.IValue)}", Align = TextAnchor.MiddleRight, Font = "robotocondensed-bold.ttf", FontSize = 16, Color = "1 1 1 0.5"}
                        }, Layer + verticalMargin + ".Prefix");

                        if (!prefix.CanUse(player))
                        {
                            container.Add(new CuiElement
                            {
                                Parent = Layer + verticalMargin + ".Prefix",
                                Components =
                                {
                                    new CuiRawImageComponent {Png            = (string) _.ImageLibrary.Call("GetImage", "CustomLocker"), Color = "1 1 1 0.6", Material = ""},
                                    new CuiRectTransformComponent {AnchorMin = "0 0.5", AnchorMax                                              = "0 0.5", OffsetMin    = "8 -8", OffsetMax = "24 8"}
                                }
                            });
                        }

                        if (playerSettings.Prefix.CValue == prefix.CValue && verticalMargin <= maxVerticalMargin)
                        {
                            maxVerticalMargin = verticalMargin;

                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-10 {verticalMargin - 16}", OffsetMax = $"-8 {verticalMargin - 2}"},
                                Image         = {Color     = "1 1 1 0.5"}
                            }, Layer + ".Prefix");

                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-150 {verticalMargin - 18}", OffsetMax = $"-8 {verticalMargin - 16}"},
                                Image         = {Color     = "1 1 1 0.5"}
                            }, Layer + ".Prefix");
                        }


                        container.Add(new CuiButton
                        {
                            RectTransform = {AnchorMin = "0 0", AnchorMax   = "1 1", OffsetMin = $"0 0", OffsetMax = $"0 0"},
                            Button        = {Color     = "0 0 0 0", Command = $"UI_WChat_Handler set prefix {Settings.PrefixSettings.Prefixes.ToList().IndexOf(prefix)}"},
                            Text          = {Text      = ""}
                        }, Layer + verticalMargin + ".Prefix");
                    }


                    //

                    if (color != null)
                    {
                        container.Add(new CuiPanel
                        {
                            RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"1 {verticalMargin - 2}", OffsetMax = $"10 {verticalMargin}"},
                            Image         = {Color     = playerSettings.PrefixColor.CValue == color.CValue ? "1 1 1 0.5" : "1 1 1 0.15"}
                        }, Layer + ".Prefix");

                        container.Add(new CuiPanel
                        {
                            RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"10 {verticalMargin - 16}", OffsetMax = $"150 {verticalMargin + 16}"},
                            Image         = {Color     = HexToRustFormat(color.CValue)}
                        }, Layer + ".Prefix", Layer + verticalMargin + ".Color");

                        if (!color.CanUse(player))
                        {
                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0 0", AnchorMax      = "1 1", OffsetMin = $"0 0", OffsetMax = $"0 0"},
                                Image         = {Color     = "0 0 0 0.9", Material = ""}
                            }, Layer + verticalMargin + ".Color");

                            container.Add(new CuiElement
                            {
                                Parent = Layer + verticalMargin + ".Color",
                                Components =
                                {
                                    new CuiRawImageComponent {Png            = (string) _.ImageLibrary.Call("GetImage", "CustomLocker"), Color = "1 1 1 0.6", Material = ""},
                                    new CuiRectTransformComponent {AnchorMin = "0.5 0.5", AnchorMax                                            = "0.5 0.5", OffsetMin  = "-8 -8", OffsetMax = "8 8"}
                                }
                            });
                        }


                        if (playerSettings.PrefixColor.CValue == color.CValue && verticalMargin <= maxVerticalMargin)
                        {
                            maxVerticalMargin = verticalMargin;

                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"8 {verticalMargin - 16}", OffsetMax = $"10 {verticalMargin - 2}"},
                                Image         = {Color     = "1 1 1 0.5"}
                            }, Layer + ".Prefix");

                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"8 {verticalMargin - 18}", OffsetMax = $"150 {verticalMargin - 16}"},
                                Image         = {Color     = "1 1 1 0.5"}
                            }, Layer + ".Prefix");
                        }

                        container.Add(new CuiButton
                        {
                            RectTransform = {AnchorMin = "0 0", AnchorMax   = "1 1", OffsetMin = $"0 0", OffsetMax = $"0 0"},
                            Button        = {Color     = "0 0 0 0", Command = $"UI_WChat_Handler set prefixColor {Settings.PrefixSettings.Colors.ToList().IndexOf(color)}"},
                            Text          = {Text      = ""}
                        }, Layer + verticalMargin + ".Color");
                    }
                }

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax = "1 0", OffsetMin = $"0 0", OffsetMax = "0 2"},
                    Image         = {Color     = "1 1 1 0.5"}
                }, Layer + ".Prefix");

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-1 {maxVerticalMargin - 2}", OffsetMax = "1 0"},
                    Image         = {Color     = "1 1 1 0.5"}
                }, Layer + ".Prefix");

                if (verticalMargin < maxVerticalMargin)
                {
                    container.Add(new CuiPanel
                    {
                        RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-1 {verticalMargin - 2}", OffsetMax = $"1 {maxVerticalMargin}"},
                        Image         = {Color     = "1 1 1 0.15"}
                    }, Layer + ".Prefix");
                }
            }


            CuiHelper.AddUi(player, container);
        }


        private static void InitializeName(BasePlayer player, bool open = true)
        {
            CuiElementContainer container      = new CuiElementContainer();
            var                 playerSettings = Handler[player.userID];

            CuiHelper.DestroyUi(player, Layer + ".Name");

            container.Add(new CuiButton
            {
                RectTransform = {AnchorMin = "0.5 0.5", AnchorMax          = "0.5 0.5", OffsetMin = $"-220 200", OffsetMax = $"70 250"},
                Button        = {Color     = "0.91 0.91 0.91 0.2", Command = "UI_WChat_Handler open name"},
                Text          = {Text      = ""}
            }, Layer + ".BG", Layer + ".Name");

            bool   isColor       = playerSettings.NameColor.CValue.Length > 0;
            string colorTag      = isColor ? $"<color={playerSettings.NameColor.CValue}>" : "";
            string colorTagClose = isColor ? $"</color>" : "";
            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 0", AnchorMax = "1 1", OffsetMin = "0 0", OffsetMax = "0 0"},
                Text =
                {
                    Text = $"{colorTag}{(playerSettings.Hide ? "МОДЕРАТОР ЧАТА" : player.displayName.ToUpper())}{colorTagClose}", Align = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 32, Color = "1 1 1 0.75"
                }
            }, Layer + ".Name");

            container.Add(new CuiLabel
            {
                RectTransform = {AnchorMin = "0 1", AnchorMax  = "1 1", OffsetMin             = "0 3", OffsetMax                        = "0 250"},
                Text          = {Text      = "ВАШЕ ИМЯ", Align = TextAnchor.LowerCenter, Font = "robotocondensed-regular.ttf", FontSize = 18, Color = "1 1 1 0.5"}
            }, Layer + ".Name");

            if (open)
            {
                float verticalMargin    = 10;
                float maxVerticalMargin = 1000;

                var orderedNameColorList = Settings.NameSettings.Colors.OrderByDescending(p => p.CanUse(player));

                for (int i = 0; i < Settings.NameSettings.Colors.Count; i++)
                {
                    var color = orderedNameColorList.ElementAtOrDefault(i);

                    verticalMargin -= 35;

                    if (color != null)
                    {
                        container.Add(new CuiPanel
                        {
                            RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"1 {verticalMargin - 2}", OffsetMax = $"10 {verticalMargin}"},
                            Image         = {Color     = playerSettings.NameColor.CValue == color.CValue ? "1 1 1 0.5" : "1 1 1 0.15"}
                        }, Layer + ".Name");

                        container.Add(new CuiPanel
                        {
                            RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"10 {verticalMargin - 16}", OffsetMax = $"150 {verticalMargin + 16}"},
                            Image         = {Color     = HexToRustFormat(color.CValue)}
                        }, Layer + ".Name", Layer + verticalMargin + ".Name.Color");

                        if (!color.CanUse(player))
                        {
                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0 0", AnchorMax      = "1 1", OffsetMin = $"0 0", OffsetMax = $"0 0"},
                                Image         = {Color     = "0 0 0 0.9", Material = ""}
                            }, Layer + verticalMargin + ".Name.Color");

                            container.Add(new CuiElement
                            {
                                Parent = Layer + verticalMargin + ".Name.Color",
                                Components =
                                {
                                    new CuiRawImageComponent {Png            = (string) _.ImageLibrary.Call("GetImage", "CustomLocker"), Color = "1 1 1 0.6", Material = ""},
                                    new CuiRectTransformComponent {AnchorMin = "0.5 0.5", AnchorMax                                            = "0.5 0.5", OffsetMin  = "-8 -8", OffsetMax = "8 8"}
                                }
                            });
                        }


                        if (playerSettings.NameColor.CValue == color.CValue && verticalMargin <= maxVerticalMargin)
                        {
                            maxVerticalMargin = verticalMargin;

                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"8 {verticalMargin - 16}", OffsetMax = $"10 {verticalMargin - 2}"},
                                Image         = {Color     = "1 1 1 0.5"}
                            }, Layer + ".Name");

                            container.Add(new CuiPanel
                            {
                                RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"8 {verticalMargin - 18}", OffsetMax = $"150 {verticalMargin - 16}"},
                                Image         = {Color     = "1 1 1 0.5"}
                            }, Layer + ".Name");
                        }

                        container.Add(new CuiButton
                        {
                            RectTransform = {AnchorMin = "0 0", AnchorMax   = "1 1", OffsetMin = $"0 0", OffsetMax = $"0 0"},
                            Button        = {Color     = "0 0 0 0", Command = $"UI_WChat_Handler set nameColor {Settings.NameSettings.Colors.ToList().IndexOf(color)}"},
                            Text          = {Text      = ""}
                        }, Layer + verticalMargin + ".Name.Color");
                    }
                }

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0 0", AnchorMax = "1 0", OffsetMin = $"0 0", OffsetMax = "0 2"},
                    Image         = {Color     = "1 1 1 0.5"}
                }, Layer + ".Name");

                container.Add(new CuiPanel
                {
                    RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-1 {maxVerticalMargin - 2}", OffsetMax = "1 0"},
                    Image         = {Color     = "1 1 1 0.5"}
                }, Layer + ".Name");

                if (verticalMargin < maxVerticalMargin)
                {
                    container.Add(new CuiPanel
                    {
                        RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-1 {verticalMargin - 2}", OffsetMax = $"1 {maxVerticalMargin}"},
                        Image         = {Color     = "1 1 1 0.15"}
                    }, Layer + ".Name");
                }

                if (IsModerator(player))
                {
                    var oldVerticalMargin = verticalMargin;

                    verticalMargin = -40 * Math.Max(Settings.PrefixSettings.Colors.Count, Settings.NameSettings.Colors.Count);
                    container.Add(new CuiPanel
                    {
                        RectTransform = {AnchorMin = "0.5 0", AnchorMax = "0.5 0", OffsetMin = $"-1 {verticalMargin}", OffsetMax = $"1 {oldVerticalMargin - 2}"},
                        Image         = {Color     = "1 1 1 0.15"}
                    }, Layer + ".Name");

                    container.Add(new CuiButton
                    {
                        RectTransform = {AnchorMin = "0.5 0", AnchorMax                          = "0.5 0", OffsetMin = $"-75 {verticalMargin - 30}", OffsetMax = $"75 {verticalMargin}"},
                        Button        = {Color     = "0.91 0.91 0.91 0.2", Command               = "UI_WChat_Handler set hide " + (playerSettings.Hide ? 0 : 1)},
                        Text          = {Text      = playerSettings.Hide ? "ВКЛ" : "ВЫКЛ", Align = TextAnchor.MiddleCenter, Font = "robotocondensed-bold.ttf", FontSize = 14, Color = "1 1 1 0.8"}
                    }, Layer + ".Name", Layer + verticalMargin + ".Name.Hide");

                    container.Add(new CuiPanel
                    {
                        RectTransform = {AnchorMin = "1 0", AnchorMax = "1 1", OffsetMin = "-20 5", OffsetMax = "-5 -5"},
                        Image         = {Color     = playerSettings.Hide ? "0.6 0.9 0.6 1" : "0.9 0.6 0.6 1"}
                    }, Layer + verticalMargin + ".Name.Hide");

                    container.Add(new CuiLabel
                    {
                        RectTransform = {AnchorMin = "0 0", AnchorMax = "1 0", OffsetMin = "-100 -50", OffsetMax = "100 -3"},
                        Text =
                        {
                            Color = "1 1 1 0.5", Text = "<b><size=14>РЕЖИМ МОДЕРАТОРА</size></b>\nВаш ник автоматически будет заменён на 'Модератор чата'\n", Align = TextAnchor.UpperCenter, Font = "robotocondensed-regular.ttf", FontSize = 12
                        }
                    }, Layer + verticalMargin + ".Name.Hide");
                }
            }

            CuiHelper.AddUi(player, container);
        }

        #endregion

        #region Commands

        [ChatCommand("r")]
        private void CmdChatPersonalReply(BasePlayer player, string command, string[] args)
        {
            if (args.Length < 1)
            {
                player.ChatMessage($"Вы <color=orange>неправильно</color> используете команду!\n<size=12>/r <сообщение> - отправить сообщение</size>");
                return;
            }

            var message = "";

            for (var i = 0; i < args.Length; i++)
                message += $"{args[i]} ";

            var target = Handler[player.userID].LastPm;
            if (target == null || !target.IsConnected)
            {
                player.ChatMessage($"Игрок не найден!");
                return;
            }

            SendPrivateMessage(player, target, message); 
        }

        [ChatCommand("ignore")]
        private void CmdChatIgnore(BasePlayer player, string command, string[] args)
        {
            if (args.Length < 1)
            {
                player.ChatMessage($"Вы <color=orange>неправильно</color> используете команду!\n<size=12>/ignore <имя> - игнорировать человека</size>");
                return;
            }

            var targetSearch = args[0].ToLower();
            var target = BasePlayer.activePlayerList.ToList().FirstOrDefault(p => p.displayName.ToLower().Contains(targetSearch));
            if (target == null || !target.IsConnected)
            {
                player.ChatMessage($"Игрок не найден!");
                return;
            }

            if (Handler[player.userID].Ignores.Contains(target.userID))
            {
                player.ChatMessage("Вы уже игнорируете этого человека, убрать игнор можно через меню - <color=orange>/chat</color>");
                return;
            }
            
            Handler[player.userID].Ignores.Add(target.userID); 
            player.ChatMessage($"Теперь вы игнорируете <color=orange>{target.displayName}</color>");
        }

        [ChatCommand("pm")]
        private void CmdChatPersonalMessage(BasePlayer player, string command, string[] args)
        {
            if (args.Length <= 1)
            {
                player.ChatMessage($"Вы <color=orange>неправильно</color> используете команду!\n<size=12>/pm <имя> <сообщение> - отправить сообщение</size>");
                return;
            }

            var targetSearch = args[0].ToLower();
            var message      = "";

            for (var i = 1; i < args.Length; i++)
                message += $"{args[i]} ";

            var target = BasePlayer.activePlayerList.ToList().FirstOrDefault(p => p.displayName.ToLower().Contains(targetSearch));
            if (target == null || !target.IsConnected)
            {
                player.ChatMessage($"Игрок не найден!");
                return;
            }
            
            SendPrivateMessage(player, target, message);
        }

        #endregion

        #region Helpers

        private void Broadcast(string text)
        {
            foreach (var player in BasePlayer.activePlayerList)
            {
                if (!Handler.ContainsKey(player.userID))
                    continue;
                    
                var targetSettings = Handler[player.userID];
                if (!targetSettings.Tips) continue;
                
                SafeMessage(player, text, Settings.AvatarId);
            }
        }
        private void MutePlayer(BasePlayer player, string initiatorName, int time, string reason)
        {
            var settings = Handler[player.userID];

            if (time == 0)
            {
                Broadcast($"<color=#90e095>{initiatorName}</color> разблокировал чат игроку <color=#e68585>{player.displayName}</color>");
                Handler[player.userID].UnMuteTime = 0;
            }
            else
            {
                Broadcast($"<color=#90e095>{initiatorName}</color> заблокировал чат игроку <color=#e68585>{player.displayName}</color>\n" +
                          $"  <size=12><color=#e3e3e3>Причина: {reason} [{TimeSpan.FromSeconds(time).ToShortString()}]</color></size>");
                Handler[player.userID].UnMuteTime = Time() + time;
            }
        }

        private void SendPrivateMessage(BasePlayer initiator, BasePlayer target, string message)
            => SendPrivateMessage(initiator.userID, target.userID, message);

        private void SendPrivateMessage(ulong initiatorReadyId, ulong targetReadyId, string message)
        {
            BasePlayer initiator = BasePlayer.FindByID(initiatorReadyId);
            BasePlayer target    = BasePlayer.FindByID(targetReadyId);

            string targetReadyName                        = BasePlayer.FindByID(targetReadyId)?.displayName ?? "UNKNOWN";
            string initiatorReadyName                     = BasePlayer.FindByID(initiatorReadyId)?.displayName ?? "UNKNOWN";
            if (initiatorReadyId == 0) initiatorReadyName = "SERVER";

            if (target == null || !target.IsConnected)
            {
                SafeMessage(initiator, "Игрок не находится на сервере!");
                return;
            }

            var targetSettings = Handler[targetReadyId];
            if (!targetSettings.Ignores.Contains(initiatorReadyId))
            {
                if (targetSettings.SoundPm)
                {
                    Effect effect = new Effect("assets/bundled/prefabs/fx/notice/item.select.fx.prefab", target, 0, new Vector3(), new Vector3());
                    EffectNetwork.Send(effect, target.Connection);
                }

                if (initiator != null && initiator.IsConnected)
                {
                    targetSettings.LastPm = initiator;
                }
                
                SafeMessage(target, $"<size=14>Личное сообщение от {initiatorReadyName}</size>\n<size=12>{message}</size>", initiatorReadyId);
            }

            SafeMessage(initiator, $"<size=14>Личное сообщение для {targetReadyName}</size>\n<size=12>{message}</size>", targetReadyId);
            AddMessage(initiatorReadyName, initiatorReadyId, message, targetReadyName, targetReadyId);
            
            DebugEx.Log((object) "[CHAT] " + initiatorReadyName + $" [{initiatorReadyId}]: > {target.displayName} [{target.userID}] :" + message); 
        }

        private void AddMessage(BasePlayer player, string message, bool team = false) => AddMessage(player.displayName, player.userID, message, team: team);

        private void AddMessage(string displayName, ulong userId, string message, string targetName = "", ulong targetId = 0UL, bool team = false)
        {
            var time       = DateTime.Now;
            var resultTime = $"{time.Hour}:{time.Minute}:{time.Second}";

            var chat = new Message
            {
                DisplayName       = displayName,
                Text              = message,
                TargetDisplayName = targetName,
                TargetUserId      = targetId.ToString(),
                Time              = resultTime,
                UserID            = userId.ToString(),
                IsTeam            = team
            };

            MessagesLogs.Add(chat);
            WebBuffer.RequestOut.SendMessage(chat);

            string logFormat = $"{displayName} [{targetId}]: {message}";
            if (team)
            {
                logFormat = "TEAM: " + logFormat;
            }
            else if (targetId != 0)
            {
                logFormat = $"{displayName} [{userId}] -> {targetName}[{targetId}]: {message}";
            }

            LogToFile($"{(!team ? targetId == 0 ? "Chat" : "PM" : "Team")}", logFormat, this);
        }

        #endregion

        #region Utils

        private static void LogInfo(string text)
        {
            if (Settings.Debug)
            {
                _.PrintWarning(text);
            }
        }
        private static bool IsModerator(BasePlayer player) => _.permission.UserHasPermission(player.UserIDString, "wchat.chatModerator");
        private static void UnloadPlugin() => _.NextTick(() => Interface.Oxide.UnloadPlugin(_.Name));
        private static List<Message> FindMessage(string input, int amount = 100)
        {
            return MessagesLogs.Where(p => p.DisplayName.ToLower().Contains(input.ToLower()) || p.UserID.ToString() == input || p.Text.ToLower().Contains(input)).Take(amount).ToList();
        }

        private static string HexToRustFormat(string hex)
        {
            if (string.IsNullOrEmpty(hex))
            {
                hex = "#FFFFFFFF";
            }

            var str = hex.Trim('#');

            if (str.Length == 6)
                str += "FF";

            if (str.Length != 8)
            {
                throw new Exception(hex);
                throw new InvalidOperationException("Cannot convert a wrong format.");
            }

            var r = byte.Parse(str.Substring(0, 2), NumberStyles.HexNumber);
            var g = byte.Parse(str.Substring(2, 2), NumberStyles.HexNumber);
            var b = byte.Parse(str.Substring(4, 2), NumberStyles.HexNumber);
            var a = byte.Parse(str.Substring(6, 2), NumberStyles.HexNumber);

            Color color = new Color32(r, g, b, a);

            return $"{color.r:F2} {color.g:F2} {color.b:F2} {color.a:F2}";
        }

        private static double Time() => DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;

        private static void SafeMessage(BasePlayer player, string text, ulong avatarId = 0)
        {
            if (player == null || !player.IsConnected) return;

            player.SendConsoleCommand("chat.add", 0, avatarId, text);
            player.SendConsoleCommand($"echo [<color=white>ЧАТ</color>] {text}");
        }

        private void RegisterPermissions()
        {
            permission.RegisterPermission("wchat.chatModerator", this);
            
            foreach (var color in Settings.NameSettings.Colors.Where(p => p.Permission.Length > 0))
            {
                if (!permission.PermissionExists(color.Permission, this))
                    permission.RegisterPermission(color.Permission, this);
            }

            foreach (var prefixes in Settings.PrefixSettings.Prefixes.Where(p => p.Permission.Length > 0))
            {
                if (!permission.PermissionExists(prefixes.Permission, this))
                    permission.RegisterPermission(prefixes.Permission, this);
            }

            foreach (var prefixes in Settings.PrefixSettings.Colors.Where(p => p.Permission.Length > 0))
            {
                if (!permission.PermissionExists(prefixes.Permission, this))
                    permission.RegisterPermission(prefixes.Permission, this);
            }

            foreach (var hooks in Settings.PrefixSettings.Hooks.Where(p => p.Permission.Length > 0))
            {
                if (!permission.PermissionExists(hooks.Permission, this))
                    permission.RegisterPermission(hooks.Permission, this);
            }

            foreach (var hooks in Settings.MessageSettings.Colors.Where(p => p.Permission.Length > 0))
            {
                if (!permission.PermissionExists(hooks.Permission, this))
                    permission.RegisterPermission(hooks.Permission, this);
            }

            PrintWarning($"Permissions initialized [{permission.GetPermissions().Count(p => p.StartsWith("wchat."))}x]");
            PrintWarning($"Moderator permission: 'wchat.chatModerator'");
        }

        #endregion
    }
}