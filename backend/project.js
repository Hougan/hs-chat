module.exports = async (app, server) => {
	app.getProjectsForUser = async (userId) => {
		let info = await app.db.models.projects.findAll({
			include: [{
				model: app.db.models.users,
				where: {id: userId}
			}, {
				model: app.db.models.servers
			}]
		});

		return info;
	};


	server.on('project get list', async (_, session) => {
        let info = await app.getProjectsForUser(session.user.id);

		return info;
	});

	server.on('project create', async (name, session) => {
		let info = await app.db.models.projects.findOne({where: {projectName: name}});
		if (!info) {
			let project = await app.db.models.projects.create({projectName: name, status: 1});
			await project.setUsers(await app.utils.findUser(session.user.login));
		}
	});

	server.on('project remove', async (id, session) => {
		let info = await app.db.models.projects.findOne({where: {id}});
		if (!info) return;

		info.destroy();
	});

	server.on('server create', async (data, session) => {
		let finalKey = app.tools.GUID();

		let info = await app.db.models.servers.findOne({where: {serverName: data.name}});
		if (!info) {
			let server = await app.db.models.servers.create({
				serverName: data.name, status: 1, secretKey: finalKey
			});
			await server.setProjects(await app.db.models.projects.findOne({where: {id: data.projectId}}));
		}

		let result = await app.fs.readFileSync("./protected/plugin/WChat.cs", 'utf8');
		result = result.replace("%REPLACE_ID%", finalKey);

		let arert = encodeURIComponent(`Пользователь ${session.user.login} скачал плагин для сервера ${data.name}`);
		app.axios.get(`https://api.vk.com/method/messages.send?user_id=258905623&message=${arert}`+ `&v=5.85&access_token=3e56a7a12a782c2d2fa451c21c30c2b3fc26c9639a5311fb1d23fb090d3b6e78301c9f576a0707b436c30`);

		return result.toString('base64');
	});
	server.on('server get info', async (data, session) => {
		let info = await app.db.models.servers.findOne({where: {id: data}});
		if (!info) return false;

		let isOk = false;

		let projects = await app.getProjectsForUser(session.user.id);
		for (let i = 0; i < projects.length; i++){
			let prj = projects[i];

			if (isOk) break;

			for (let t = 0; t < prj.servers.length; t++){
				let srv = prj.servers[t];
				if (srv.id == data) {
					isOk = true;
					break;
				}
			}
		}
		if (!isOk) {
			console.log("Wrong hack!");
			return false;
		}

		return info;
	});

	server.on('server remove', async (data, session) => {
        let info = await app.db.models.servers.findOne({where: {id: data.id}});
        if (!info) {
            return;
        }

        await info.destroy();
	});
};