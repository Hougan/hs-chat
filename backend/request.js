module.exports = async (app, server) => {
	app.exportBuffer = {};
	app.resultBuffer = {};
	app.messageBuffer = {};

	app.pause = async (time) => new Promise((callback) => { setTimeout(() => callback(true), time * 1000)});

	server.on('BACKEND_FETCH_LOGS', async (serverId, session) => {
		let serverInfo = await app.db.models.servers.findOne({ where: { id: serverId }});
		if (!serverInfo) {
			console.log('Wrong server!');
			return false;
		}
		let isOk = false;

		let projects = await app.getProjectsForUser(session.user.id);
		for (let i = 0; i < projects.length; i++){
			let prj = projects[i];

			if (isOk) break;

			for (let t = 0; t < prj.servers.length; t++){
				let srv = prj.servers[t];
				if (srv.id == serverId) {
					isOk = true;
					break;
				}
			}
		}
		if (!isOk) {
			console.log("Wrong hack!");
			return false;
		}

		if (!app.exportBuffer[serverInfo.secretKey])
		{
			app.exportBuffer[serverInfo.secretKey] = {};
		}

		let id = app.tools.GUID();
		app.exportBuffer[serverInfo.secretKey][id] = {
			finish: false,
			request: {
				requestType: 'in_chatHistoryRequest',
				requestParams: {
					id: id,
					amount: 100
				}
			}
		};

		let repeat = 0;
		while (!!app.exportBuffer[serverInfo.secretKey][id]){
			if (repeat >= 3){
				delete app.exportBuffer[serverInfo.secretKey][id];
				return false;
			}

			await app.pause(1);
			repeat++;
		}

		let info = app.resultBuffer[serverInfo.secretKey][id];
		return info;
	});
	server.on('BACKEND_SEND_MESSAGE', async (data, session) => {
		let serverInfo = await app.db.models.servers.findOne({ where: { id: data.serverId }});
		if (!serverInfo) {
			console.log('Wrong server!');
			return false;
		}
		let isOk = false;

		let projects = await app.getProjectsForUser(session.user.id);
		for (let i = 0; i < projects.length; i++){
			let prj = projects[i];

			if (isOk) break;

			for (let t = 0; t < prj.servers.length; t++){
				let srv = prj.servers[t];
				if (srv.id == data.serverId) {
					isOk = true;
					break;
				}
			}
		}
		if (!isOk) {
			console.log("Wrong hack!");
			return false;
		}

		if (!app.exportBuffer[serverInfo.secretKey])
		{
			app.exportBuffer[serverInfo.secretKey] = {};
			app.resultBuffer[serverInfo.secretKey] = {};
		}

		let id = app.tools.GUID();
		app.exportBuffer[serverInfo.secretKey][id] = {
			finish: false,
			request: {
				requestType: 'in_sendMessageRequest',
				requestParams: {
					id: id,
					message: data.message,
					target: !data.target ? '' : data.target.userId
				}
			}
		};

		console.log(`[${id.substr(0, 5)}] Creating request`);

		let repeat = 0;
		while (!!app.exportBuffer[serverInfo.secretKey][id]){
			if (repeat >= 3){
				delete app.exportBuffer[serverInfo.secretKey][id];
				return false;
			}

			await app.pause(1);
			repeat++;
		}

		console.log(`[${id.substr(0, 5)}] Answered`);

		let info = app.resultBuffer[serverInfo.secretKey][id];
		return info;
	});
	server.on('BACKEND_GET_MESSAGES', async (data, session) => {
		let serverInfo = await app.db.models.servers.findOne({ where: { id: data.serverId }});
		if (!serverInfo) {
			console.log('Wrong server!');
			return false;
		}
		let isOk = false;

		let projects = await app.getProjectsForUser(session.user.id);
		for (let i = 0; i < projects.length; i++){
			let prj = projects[i];

			if (isOk) break;

			for (let t = 0; t < prj.servers.length; t++){
				let srv = prj.servers[t];
				if (srv.id == data.serverId) {
					isOk = true;
					break;
				}
			}
		}
		if (!isOk) {
			console.log("Wrong hack!");
			return false;
		}

		if (!app.messageBuffer[serverInfo.secretKey])
		{
			app.messageBuffer[serverInfo.secretKey] = [];
		}
		if (app.messageBuffer[serverInfo.secretKey].length == 0){
			console.log('Empty message cache');
			return [];
		}

		let info = app.messageBuffer[serverInfo.secretKey];
		app.messageBuffer[serverInfo.secretKey] = [];

		return info;
	});
	server.on('BACKEND_GET_INFO', async (data, session) => {
		let serverInfo = await app.db.models.servers.findOne({ where: { id: data.serverId }});
		if (!serverInfo) {
			console.log('Wrong server!');
			return false;
		}
		let isOk = false;

		let projects = await app.getProjectsForUser(session.user.id);
		for (let i = 0; i < projects.length; i++){
			let prj = projects[i];

			if (isOk) break;

			for (let t = 0; t < prj.servers.length; t++){
				let srv = prj.servers[t];
				if (srv.id == data.serverId) {
					isOk = true;
					break;
				}
			}
		}
		if (!isOk) {
			console.log("Wrong hack!");
			return false;
		}

		if (!app.exportBuffer[serverInfo.secretKey])
		{
			app.exportBuffer[serverInfo.secretKey] = {};
			app.resultBuffer[serverInfo.secretKey] = {};
		}

		let id = app.tools.GUID();
		app.exportBuffer[serverInfo.secretKey][id] = {
			finish: false,
			request: {
				requestType: 'in_userInfoRequest',
				requestParams: {
					id: id,
					targetId: data.targetId
				}
			}
		};

		console.log(`[${id.substr(0, 5)}] Creating request`);

		let repeat = 0;
		while (!!app.exportBuffer[serverInfo.secretKey][id]){
			if (repeat >= 3){
				delete app.exportBuffer[serverInfo.secretKey][id];
				return false;
			}

			await app.pause(1);
			repeat++;
		}

		console.log(`[${id.substr(0, 5)}] Answered`);

		let info = app.resultBuffer[serverInfo.secretKey][id];
		return info;
	});
	server.on('BACKEND_MUTE_PLAYER', async (data, session) => {
		let serverInfo = await app.db.models.servers.findOne({ where: { id: data.serverId }});
		if (!serverInfo) {
			console.log('Wrong server!');
			return false;
		}
		let isOk = false;

		let projects = await app.getProjectsForUser(session.user.id);
		for (let i = 0; i < projects.length; i++){
			let prj = projects[i];

			if (isOk) break;

			for (let t = 0; t < prj.servers.length; t++){
				let srv = prj.servers[t];
				if (srv.id == data.serverId) {
					isOk = true;
					break;
				}
			}
		}
		if (!isOk) {
			console.log("Wrong hack!");
			return false;
		}

		if (!app.exportBuffer[serverInfo.secretKey])
		{
			app.exportBuffer[serverInfo.secretKey] = {};
			app.resultBuffer[serverInfo.secretKey] = {};
		}

		console.log(data);
		let id = app.tools.GUID();
		app.exportBuffer[serverInfo.secretKey][id] = {
			finish: false,
			request: {
				requestType: 'in_userMuteRequest',
				requestParams: {
					id: id,
					targetId: data.targetId,
					time: data.time,
					reason: data.reason
				}
			}
		};

		console.log(app.exportBuffer[serverInfo.secretKey][id]);

		console.log(`[${id.substr(0, 5)}] Creating request`);

		let repeat = 0;
		while (!!app.exportBuffer[serverInfo.secretKey][id]){
			if (repeat >= 3){
				delete app.exportBuffer[serverInfo.secretKey][id];
				return false;
			}

			await app.pause(1);
			repeat++;
		}

		console.log(`[${id.substr(0, 5)}] Answered`);

		let info = app.resultBuffer[serverInfo.secretKey][id];
		return info;
	});
};