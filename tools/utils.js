module.exports = async (app, server) => {
	app.pluginVersion      = "0.2.4";

	app.utils              = {};
	app.utils.findUser     = async (login) => {
		let info = await app.db.models.users.findOne({where: {login}});
		return info;
	};
	app.utils.getFullUser  = async (login) => {
		let info = await app.db.models.users.findOne({
			where: {login}, attributes: ['id', 'login', 'status', 'data'], include: [{
				model   : app.db.models.projects,
				required: false,
				include : [{
					model   : app.db.models.servers,
					required: false
				}]
			}]
		});

		return info;
	};
	app.utils.fetchRequest = async (request) => {
		if (!request.secretKey) return "Указан неверный ключ, скачайте плагин ещё раз!";

		let serverInfo = await app.db.models.servers.findOne({where: {secretKey: request.secretKey}});
		if (!serverInfo) return "Указан неверный ключ, скачайте плагин ещё раз!";

		console.log(`UTILS: [${request.requestParams.id.substr(0, 5)}] Got answer: '${request.requestType}'`);

		if (!app.exportBuffer[request.secretKey]) {
			app.exportBuffer[request.secretKey] = {};
		}
		if (!app.resultBuffer[request.secretKey]) {
			app.resultBuffer[request.secretKey] = {};
		}
		if (!app.messageBuffer[request.secretKey]) {
			app.messageBuffer[request.secretKey] = [];
		}

		switch (request.requestType) {
			case "out_validateVersion": {
				if (request.requestParams.version != app.pluginVersion)
					return `Вышло новое обновление на плагин [${request.requestParams.version} -> ${app.pluginVersion}], старый плагин не поддерживается!`;

				return "";
			}
			case "out_requestListRequest": {
				await app.db.models.servers.update({ fps: request.requestParams.fps, online: request.requestParams.online }, { where: { secretKey: request.secretKey }});

				let keys = Object.keys(app.exportBuffer[request.secretKey]);
				if (keys.length === 0) return "NO COMMANDS TO EXECUTE";

				return JSON.stringify(app.exportBuffer[request.secretKey][keys[0]].request);
				break;
			}
			case "out_chatHistoryAnswer": {
				delete (app.exportBuffer[request.secretKey][request.requestParams.id]);
				app.resultBuffer[request.secretKey][request.requestParams.id] = request.requestParams.messages;
				break;
			}
			case "out_sendMessageRequest": {
				app.messageBuffer[request.secretKey].push(request.requestParams.message);
				break;
			}
			case "out_sendMessageAnswer": {
				delete (app.exportBuffer[request.secretKey][request.requestParams.id]);
				app.resultBuffer[request.secretKey][request.requestParams.id] = request.requestParams.result;
				break;
			}
			case "out_userInfoAnswer": {
				delete (app.exportBuffer[request.secretKey][request.requestParams.id]);
				app.resultBuffer[request.secretKey][request.requestParams.id] = request.requestParams.info;
				break;
			}
			case "out_userMuteAnswer": {
				delete (app.exportBuffer[request.secretKey][request.requestParams.id]);
				app.resultBuffer[request.secretKey][request.requestParams.id] = request.requestParams.result;
				break;
			}
			default: {
				return "Unknown type " + request.requestType;
				break;
			}
		}

		return "OK";
	};

};