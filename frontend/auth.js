import 'vuetify/dist/vuetify.min.css';
import '@babel/polyfill';
import '@mdi/font/css/materialdesignicons.css'

import Vue from 'vue';

import VueRouter from 'vue-router';
import '@mdi/font/css/materialdesignicons.min.css';
import IO from 'socket.io-client';
import WebApi from 'osmium-webapi';
import tools from 'osmium-tools';

Vue.use(VueRouter);


window.tools = tools;
window.webApi = new WebApi.WebApiClient(IO('/'));

import App  from './components/auth.vue';
import Auth from './components/auth/auth.vue';

let router = window.router = new VueRouter({
	mode: 'history',
	routes: [{
		path     : '/auth',
		component: Auth
	}]
});

import vuetify from '../plugins/vuetify'
new Vue({
	vuetify,
	router,
	render: h => h(App),
	async mounted() {
		await window.webApi.ready();
		this.$children[0].webApi = window.webApi;
		if (this.$children[0].webApiReady) this.$children[0].webApiReady();
	}

}).$mount('#app');
